interface QuestionnaireService {
    fetchAllBankInfo: (applicationNumber: string) => Promise<QuestionnaireBankInfoResponse>;
    removeBankInfo: (applicationNumber: string) => Promise<QuestionnaireBankInfoResponse>;
}

interface QuestionnaireBankInfoResponse {
    [bankName: string]: Array<QuestionnaireAccount>;
}

interface QuestionnaireAccount {
    bankName: string;
    accountType: string;
    accountNumber: string;
    currentBalance: string;
    bankInfoId: string;
    accountHolderName: string;
}

interface FetchQuestionnaireAccountsResult {
    accounts: Array<QuestionnaireAccount>
}

interface FetchQuestionnaireAccounts extends polymer.Base {
    execute: (applicationNumber: string) => Promise<FetchQuestionnaireAccountsResult | Error>
    require: Array<string>;
}

interface UIAccount {
    name: string;
    number: string;
    holder: string;
    type: string;
    currentBalance: number;
    bankInfoId: string;
}