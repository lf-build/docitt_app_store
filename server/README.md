# App Registry
*File server for Alloy that pulls either from Alloy project folder or from integrated s3 service*

## Run locally with alloy file structure

*lf_alloy folder must be present in the same parent folder as the app registry*

`node server.js local`

## Run with s3 integration
`node server.js`