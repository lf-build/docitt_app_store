var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var routes = require(__dirname + '/routes.js');

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// parse application/json
app.use(bodyParser.json());

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));

app.use('/app', routes);

app.listen(9000, function (err) {
    if (err) {
        console.error("===> An error occurred when starting the server", err);
        process.exit();
    }

    console.log('listening on port 9000');
});