var express = require('express');
var path = require('path');
var router = express.Router();
var fs = require('fs');

router.get('/:tenant/:path(*)', function (req, res) {
    var tenant = req.params.tenant;
    var localPath = __dirname + '/../app/components/';
    var lendfoundryPath = localPath + 'lendfoundry/' + req.params.path;
    var tenantPath = localPath + tenant + '/' + req.params.path;

    fs.access(tenantPath, fs.constants.F_OK, (err) => {
        if (err) {
            fs.access(lendfoundryPath, fs.constants.F_OK, (err) => {
                if (!err) {
                    return res.sendFile(path.resolve(lendfoundryPath));
                } else {
                    res.status(404).send("File not found.");
                }
            });
        } else {
            return res.sendFile(path.resolve(tenantPath));
        }
    });
});

module.exports = router;