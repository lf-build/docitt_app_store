'use strict';

module.exports = function (grunt) {

  var path = require('path');
  var fs = require('fs');

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    minifyPolymer: 'grunt-minify-polymer'
  });


  // Configurable paths for the application
  var appConfig = {
    app: 'app',
    dist: 'dist'
  };

  // Define the configuration for all the tasks
  grunt.initConfig({
    // Project settings
    yeoman: appConfig,

    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'images/{,*/}*.{webp}',
            'styles/fonts/{,*/}*.*',
            'components/**/*.*',
            'favicons/**/*.*',
            'config.js',
            'styles/**/*'
          ]
        }]
      }
    },

    minifyPolymer: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>/components/',
          src: ['**/*.html', '!(polymer)/*.html'],
          dest: '<%= yeoman.dist %>/components/'
        }]
      }
    },
    copyTenant: {
      dist: {
        src: ['<%= yeoman.dist %>/components']
      }
    }

  });

  grunt.registerMultiTask('copyTenant', 'Copy to all tenant', function () {

    // Loop over each src directory path provided via the configs src array.
    this.data.src.forEach(function (dir) {
         // Log if the directory src path provided cannot be found.
         if (!grunt.file.isDir(dir)) {
          grunt.fail.warn('Directory not found: '.yellow + dir.yellow);
        }
  
        // Append a forward slash If the directory path provided
        // in the src Array does not end with one.
        if (dir.slice(-1) !== '/') {
          dir += '/';
        }
  
        // Generate the globbin pattern (only one level deep !).
        var glob = [dir, '*'].join('');
  
        // Create an Array of all top level folders (e.g. site-*)
        // in the dist directory and exclude the assets directory.
        var dirs = grunt.file.expand(glob).map(function(dir) {
          return dir;
        });
  
        var files = grunt.file.expand([dir + '/lendfoundry/**/*.*'])
          .map(function(file) {
            return {
              file: file,
              originalFolder: file.replace(path.join(dir, 'lendfoundry'),'')
            };
          });

        // Loop over each directory.
        dirs.forEach(function(dir) {
          // Add the folders to exclude here.
          if (dir.endsWith('/lendfoundry')) {
            return;
          }

          files.forEach(function(file) {
            if(fs.existsSync(path.join(dir,file.originalFolder))===false){
              grunt.file.copy(file.file,path.join(dir,file.originalFolder));
            }
          });
        });
    });
  });
  
  grunt.registerTask('build', [
    'clean:dist',
    'copy:dist',
    'minifyPolymer:dist',
    'copyTenant'
  ]);
};