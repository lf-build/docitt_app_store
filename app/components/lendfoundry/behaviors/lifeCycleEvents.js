(function () {
  var ensure = {
    /**
     * Ensure a service component has been loaded. Resolves with a reference to the instance when ready or with
     * an object with the service component id for keys and a reference to the service component for values.
     * @param {...string} id The shadow dom id attribute of the service component child
     * @returns {Promise<HTMLElement|Object>}
     * @example
     * // ensuring a single dependency
     * this.ensure("someService").then(function (someService) { someService.someMethod(); });
     * @example
     * // ensuring multiple dependencies
     * this.ensure("firstService", "secondService").then(function (services) {
             *      services.firstService.method();
             *      services.secondService.method();
             * });
     *
     */
    ensure: function () {
      var self = this;
      var args = Array.prototype.slice.call(arguments);

      var ensuredDependencies = args.map(function (arg) {
        return new Promise(function (resolve) {
          var timeout;
          var sub;
          var dependency;

          if (arg instanceof HTMLElement)
            dependency = arg
          else if (typeof arg === "string")
            dependency = $("#" + arg, self)[0];

          if (!dependency)
            return console.error("Unable to locate dependency " + arg + " in ", self)

          if (dependency && dependency.is)
            return resolve(dependency);

          timeout = setTimeout(function () {
            console.error("Dependency " + arg + " timed out in ", self)
          }, 3000)

          sub = self.subscribe(arg + ".attached", function () {
            resolve(dependency)
            window._eventHub.unsubscribe(sub)
            window.clearTimeout(timeout)
          });
        })
      });

      return Promise.all(ensuredDependencies)
        .then(function (dependencies) {
          if (dependencies.length === 1) return dependencies[0];

          return dependencies.reduce(function (context, current) {
            context[current.id] = current;
            return context;
          }, {})
        });
    }
  }

  var publishLifeCyleEvents = {
    ready: function () {
      if (!this.publish || !this.subscribe) {
        console.error("PublishLifecyleEventsBehavior requires eventHub behavior");
        return;
      }

      this.publish(this.id + ".ready", {instance: this});
    },

    attached: function () {
      if (!this.publish || !this.subscribe) {
        console.error("PublishLifecyleEventsBehavior requires eventHub behavior");
        return;
      }

      this.publish(this.id + ".attached", {instance: this});
    }
  }

  var require = {
    created: function () {
      if (!this.attached)
        return

      if (!this.require)
        return

      var originalAttached = this.attached.bind(this)

      this.attached = function () {
        this.ensure.apply(this, this.require)
          .then(function () {
            originalAttached()
          })
      }
    }
  }

  /**
   * @name alloy.lifeCycleEvents
   * @mixin
   * @type {{ensure: function}}
   */
  window.alloy.lifeCycleEvents = [window.eventHub, ensure, publishLifeCyleEvents, require]
})()