window.alloy.actions = {
    listeners: {
        "alloy-navigate": "handleNavigate"
    },

    handleNavigate: function (event) {
        var destination = _.get(event, "detail.actionDestination")

        if (!destination)
            return console.warn("Unable to navigate to " + destination)

        window.location.hash(destination)
    },

    fireAction: function (event) {
        var action = $(event.target).attr("action")
        var detail = {}

        if (_.isUndefined(action))
            return

        // include attributes that begin with 'action' in event details
        $.each(event.target.attributes, function () {

            if (!this.specified)
                return

            if (!this.name.includes("action"))
                return

            detail[_.camelCase(this.name)] = this.value
        })

        event.stopPropagation()

        this.fire(action, detail)
    }
}