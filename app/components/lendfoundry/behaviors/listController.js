window.alloy.listController = {
  properties: {
    filter: {
      type: String,
      value: ""
    },
    items: {
      type: Array,
      value: function () {
        return []
      }
    },
    filteredItems: {
      type: Array,
      notify: true
    }
  },

  setItems: function (items) {
    this.items = items;
    this.filteredItems = this.computeFilteredItems(this.items, this.filter)
  },

  setFilter: function (filter) {
    this.filter = this._cleanString(filter)
    this.filteredItems = this.computeFilteredItems(this.items, this.filter)
  },

  computeFilteredItems: function (items, filter) {
    if (filter === "" || typeof filter === "undefined") return items;

    return items.filter(function (item) {
      return this._cleanString(item.value).includes(filter)
    }.bind(this))
  },

  _cleanString: function (string) {
    return string.replace(/ /g, "").toLowerCase();
  }
}