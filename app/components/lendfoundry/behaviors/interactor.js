(function () {
  var interactor = {
    attached: function () {
      var originalExecute = this.execute.bind(this);

      if (!this.require || this.require.length === 0) {
        return
      }

      this.execute = function (context) {
        return this.ensure.apply(this, this.require)
        .then(function () {
          return originalExecute(context)
        })
      }
    }
  }

  window.alloy.interactor = [eventHub, window.alloy.lifeCycleEvents, interactor]
})()