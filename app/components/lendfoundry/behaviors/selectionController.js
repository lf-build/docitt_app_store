window.alloy.selectionController =     {
  properties: {
    selection: {type: String, notify: true}
  },

  select: function (value) {
    this.selection = value;
  },

  clear: function () {
    this.value = undefined
  }
};