(function () {
  window.alloy.visibilityController = {
    properties: {
      visibilityModes: {
        type: Object,
        value: function () {
          return {
            default: "DEFAULT"
          }
        }
      },

      activeVisibilityMode: {
        type: String,
        value: "DEFAULT"
      }
    },

    changeVisibilityMode: function (nextVisibilityMode) {
      if (!_.has(this.visibilityModes, nextVisibilityMode))
        console.error(
          "Unable to set visibility mode to " + nextVisibilityMode + ".",
          "Ensure desired visibility mode is one of ", _.values(this.visibilityModes))

        this.activeVisibilityMode = nextVisibilityMode
    },

    isActiveMode: function (visibilityMode) {
      return this.activeVisibilityMode === visibilityMode
    }
  }
})()