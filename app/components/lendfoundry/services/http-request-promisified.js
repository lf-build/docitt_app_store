(function () {
    var $httpRequest = {
        is: "http-request-promisified",
        get: function () {
            return Promise.resolve(this.$.http.get.apply(this.$.http, arguments));
        },
        post: function () {
            return Promise.resolve(this.$.http.post.apply(this.$.http, arguments));
        },
        put: function () {
            return Promise.resolve(this.$.http.put.apply(this.$.http, arguments));
        },
        "delete": function () {
            return Promise.resolve(this.$.http["delete"].apply(this.$.http, arguments));
        }
    };
    Polymer($httpRequest);
})();
