Polymer({
    is: "offer-engine-service",

    behaviors: [eventHub, publishLifecycleEventsBehavior],

    properties: {
        baseUrl: {
            value: null
        },
    },
    attached: function () {
        if(this.configuration.services.offerEngine == undefined)
        {
            throw new Error("offerEngine service not found");
        }
        var self = this;;
        self.baseUrl = this.configuration.services.offerEngine;
    },
    createOffer: function (type, applicationNumber) {
        var url = [type, applicationNumber].join("/");
        return this.$.$http.post(url, {}, this.baseUrl);
    },

    fetchSelectedOffer: function (type, applicationNumber) {
        var url = [type, applicationNumber, "selected"].join("/");

        return this.$.$http.get(url, this.baseUrl);
    },

    getOffersByApplicationNumber: function (applicationNumber) {
        var url = [applicationNumber].join("/");
        return this.$.$http.get(url, this.baseUrl);
    },

    getSelectedOffer: function (offers) {
        return _.filter(offers.offers, function (offer) {
            return offer.selected;
        })[0];
    },

    getPreferredOffer: function (offer) {
        var preferredOfferId = offer.preferred;
        return this.getOfferById(offer.offers, preferredOfferId) || offer.offers[0];
    },

    getOfferById: function (offers, id) {
        return offers.filter(function (offer) {
            return offer.id === id.toString();
        })[0];
    },

    getOfferType: function (response) {
        return response.offerConfiguration.type;
    },

    getApplicationNumber: function (response) {
        return response.applicationNumber;
    },

    /**
     * Get a new offer based on a new amount
     * @param {string} type
     * @param {string} applicationNumber
     * @param {string} nextAmount
     * @returns {Promise<any>}
     */
    changeOffer: function (type, applicationNumber, nextAmount) {
        var url = [type, applicationNumber, "change", nextAmount].join("/");
        return this.$.$http.post(url, {}, this.baseUrl);
    },

    /**
     * Select an offer by index
     * @param offerType
     * @param appId
     * @param offerIndex
     * @returns {Promise<any>}
     */
    selectOffer: function (offerType, appId, offerIndex) {
        var url = [offerType, appId, offerIndex, "select"].join("/");
        return this.$.$http.post(url, {}, this.baseUrl);
    },

    /**
     * Select the preferred offer indicated by the offers object
     * @param {string} type
     * @param {string} applicationNumber
     * @param {string} offers
     * @returns {Promise<any>}
     */
    selectPreferredOffer: function (type, applicationNumber, offers) {
        var offerIndex = offers.preferred;
        return this.selectOffer(type, applicationNumber, offerIndex);
    },

    /**
     * Retrieve the offer associated with an application
     * @method getByApplication
     * @param {string} id The id of the application whose offer you want
     * @returns {Promise<Object>}
     */
    getByApplication: function (id) {
        return this.$.$http.get(id, this.baseUrl);
    }
});
