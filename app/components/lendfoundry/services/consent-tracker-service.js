Polymer({
    is: "consent-tracker-service",

    properties: {
        baseUrl: {
            value: null,
            type: String
        }
    },
    attached: function () {
        var self = this;
        if(this.configuration.services.consentTracker == undefined){
             throw new Error("consentTracker service not found");
        }
        self.baseUrl = this.configuration.services.consentTracker;
    },

    /**
     * Saves consent given by various entities (borrowers, merchants, etc...)
     * @param {string} consentType
     * @param {string|number} entityId
     * @param {string} entityType
     * @returns {Promise<any>}
     */
    accept: function (consentType, entityId, entityType) {
        var url = "/" + entityType;
        var payload = {
            entityId: entityId,
            consent: consentType
        };
        return this.$.http.post(url, payload, this.baseUrl);
    },

    configureActivityLog: function () {
        return this.$.http.post();
    }
});
