Polymer(/** @class ui-selector */{
  is: "ui-selector",

  properties: {
    buttonText: {
      type: String,
      value: "Select..."
    },

    filter: {
      type: String,
      value: "",
      observer: "filterOptions"
    },
  filterHandler: String,
    /**
     * This will display arrow in button
     */
    displayArrow: {
      type: Boolean,
      value: false
    }
  },

  created: function () {
    this.toggleClass("btn-group", true)
  },

  attached: function () {
    this.filter="";
    $(this.$.optionsContainer).find("#accountFilter").val('');
  },
  init:function(){
  },

  filterOptions: function (nextFilter) {
    var self= this;
      this.debounce(this.filterOptions, function () {
     var filter = nextFilter.toUpperCase();
    
 if (this.filterHandler
                    && typeof this.domHost[this.filterHandler] === "function")
                   this.domHost[this.filterHandler].call(this.domHost,filter).then(function(response){
                   $("[option]", this.$.optionsContainer).each(function () {
      this.innerHTML.toUpperCase().indexOf(filter) > -1
        ? $(this).show()
        : $(this).hide()
    })
                   });
                  
    
    }, 1000);

   
   

    function showAll() {
      $("[option]", this.$.optionsContainer).each(function () {
        $(this).show()
      })
    }
  },

  attributeChanged: function () {
    this.setStyles()
  },

  setStyles: function () {
    var button = this.$.button
    var options = this.$.optionsContainer
    var dropdownIsOpen = this.classList.contains("open")
    var dropdownIsClosed = !dropdownIsOpen
    var isDropup = this.classList.contains("dropup")

    // Restore styling if closed
    if (dropdownIsClosed) {
      button.style.borderTopLeftRadius = "4px"
      button.style.borderTopRightRadius = "4px"
      button.style.borderBottomRightRadius = "4px"
      button.style.borderBottomRightRadius = "4px"
      options.style.borderBottomRightRadius = "4px"
      options.style.borderBottomLeftRadius = "4px"
      options.style.borderTopLeftRadius = "4px"
      options.style.borderTopRightRadius = "4px"
    }

    // Square off appropriate corners if open and a dropup
    if (dropdownIsOpen && isDropup) {
      button.style.borderTopLeftRadius = 0
      button.style.borderTopRightRadius = 0
      options.style.borderBottomLeftRadius = 0
      options.style.borderBottomRightRadius = 0
    }

    // Square off corners when open
    if (dropdownIsOpen && !isDropup) {
      button.style.borderBottomLeftRadius = 0
      button.style.borderBottomRightRadius = 0
      options.style.borderTopLeftRadius = 0
      options.style.borderTopRightRadius = 0
    }
  }
})
