Polymer(/** @class ui-selector */{
  is: "ui-datalist",

  properties: {
    filter: {
      type: String,
      value: "",
      observer: "filterChanged"
    },
    _open: {
      type: Boolean,
      value: false,
    },
     filterHandler: String
  },

  attached: function () {
    this.input = $("input", this)[0]
    this.keyPressed = this.keyPressed.bind(this)

    // $(this.input).on("keydown keyup", this.keyPressed)
    $(this.input).on("keydown keyup", function (e) {
      if (e.keyCode === 38 || e.keyCode === 40)
        e.preventDefault()
    })

    var self = this;
    $(this).on("click", function (e) {
      if ($(e.target).attr("option"))
        self.close()
    })
    //close the data-list if clicked outside.
    $(document).mouseup(function (){
        self.close();
    });
  },

  detached: function () {
    this.removeEventListener("keydown", this.keyPressed)
  },

  keyPressed: _.debounce(function (e) {
    var ARROW_UP = 38
    var ARROW_DOWN = 40
    var ESCAPE = 27
    var ENTER = 13
    var KEY_CODE = e.keyCode
    var $hoveredOption;
    var $options = this._getOptions()
    var nextOpt

    $options.each(function () {
      if (this.classList.contains("is-hovered"))
        $hoveredOption = $(this)

      this.classList.remove("is-hovered")
    })

    switch (KEY_CODE) {
      case ESCAPE:
        return this.close()
      case ENTER:
        if (!this._open) return

        $hoveredOption && $hoveredOption.trigger("click");
        this.close()
        return

      case ARROW_DOWN:
        e.preventDefault();
        nextOpt = this._getNext($hoveredOption)
        hover(nextOpt)
        return

      case ARROW_UP:
        e.preventDefault();
        nextOpt = this._getPrev($hoveredOption)
        hover(nextOpt)
        return

      default:
        return
    }

    function hover($el) {
      $($el).addClass("is-hovered")
    }
  }, 10),

  _getNext: function (el) {
    var $options = this._getVisibleOptions()

    if (!el)
      return this._getDefaultOption()
    else
      return $options.filter(function (idx) {
        return idx > $options.index(el)
      }).first()[0] || this._getFirstOption()
  },

  _getPrev: function (el) {
    var $options = this._getOptions().filter(function () { return this.style.display !== "none" })

    if (!el)
      return this._getDefaultOption()
    else
      return $options.filter(function (idx) {
        return idx < $options.index(el)
      }).last()[0] || this._getLastOption()
  },

  filterChanged: function (nextFilter) {
    var self= this;
    
    this.debounce(this.filterChanged, function () {
     var filter = nextFilter.toUpperCase();
    
 if (this.filterHandler
                    && this.domHost && typeof this.domHost[this.filterHandler] === "function")
                    {
                   this.domHost[this.filterHandler].call(this.domHost,filter).then(function(response){
                   self.updateListVisibility(filter)
    self.updateOptionVisibility(filter)
                   });
                    }
                    else
                    {
                      self.updateListVisibility(filter)
    self.updateOptionVisibility(filter)
                    }
    
    }, 1000);
    
  },

  updateOptionVisibility: function (filter) {
    if(filter)
      $(this.$.optionsList).scrollTop($(this.$.optionsList.firstElementChild).position().top);
    
    this._getOptions().each(function () {
      var matchesFilter = this.innerHTML.toUpperCase().indexOf(filter) !== -1

      if (matchesFilter)
        this.style.display = "block"
      else
        this.style.display = "none"
    })
  },

  updateListVisibility: function (filter) {
    var self = this;
    var matches = $("[option]", this).filter(function () {
      return this.innerHTML.toUpperCase().indexOf(filter) > -1
    })

    if (filter.length === 0) return this.close();

    if (filter.length > 0) {
      if (matches.length > 0) return this.open()

      // If Matches length is 0 then again check for search response and fill the array, to handle search input.
      if (matches.length === 0) {
        this.domHost[this.filterHandler].call(this.domHost,filter).then(function(response){
          self.set("filterInstitutions", []);
          self.set("filterInstitutions", response);
          if(response !== null && response.length > 0) return self.open();
          else return self.close();
        });
      }
    }
  },

  _getOptions: function () {
    return $("[option]", this)
  },

  _getVisibleOptions: function () {
    return this._getOptions().filter(function () {
      return this.style.display !== "none"
    })
  },

  _getDefaultOption: function () {
    return this._getVisibleOptions().last()
  },

  _getFirstOption: function () {
    return this._getVisibleOptions().first()
  },

  _getLastOption: function () {
    return this._getVisibleOptions().last()
  },

  close: function () {
    this._open = false
    this.toggleClass("is-open", false)
    this.$.outerInput.focus()
  },

  open: function () {
    this._open = true
    this.toggleClass("is-open", true)
    this.$.innerInput.focus()
  }
})
