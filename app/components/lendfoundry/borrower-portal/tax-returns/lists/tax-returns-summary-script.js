(function () {
    Polymer({
        is: 'tax-returns-summary-list',

        behaviors: [eventHub, ensureDependenciesBehavior],

        properties: {
            titleText: String, /*Passed from parent for scene title*/
            titlename:{
                type:String,
                value : "Owner"
            },
            entityName:{
                type:String,
                value : "TaxReturn"
            },
            subtitleText: String, /*Passed from parent for scene title*/
            customerAccounts: { type: Array, value: [] }, /*Summary rows passed from parent*/
            busy: Boolean,
            disableClass: { type: String, value: "" },
            institutions: Array, /*List of institutions to be displayed in bottom dropdown*/
            customerInstitutions: Array,  /*List of customer institutions to be displayed*/
            inProgress: Boolean, /*This will be used to disable buttons*/
            institutionsWithTaxReturns: { type: Array, value: [] },
            customerId: { type: String, value: "" },
            applicationNumber: String,
            institutionType: { type: String, value: 'TaxReturn' }
        },

        behaviors: [eventHub, ensureDependenciesBehavior, alloy.lifeCycleEvents, alloy.actions],

        observers: ['taxReturnsUpdated(customerInstitutions)'],

        computeIsEmpty: function (array) {
            if (!array)
                return true;
            return array.length === 0;
        },

        _displayLogo: function (value) {
            if (value === 0) return true;
            else return false;
        },

        getDataId: function (parent_index, child_index) {
            return parent_index + '-' + child_index;
        },
        checkoutLength: function(documentList){
            return (documentList[0].documents.length > 1 && documentList[0].documents != null);
        },
        handleDownloadDocumentClick: function (e) {
            var self = this;
            var entityType = "filethis";
            var documentId = e.target.dataset.id || "";
            if (!documentId) {
                console.warn("document id is blank");
                return;
            }
            var entityId = e.target.dataset.entityId;
            entityId = encodeURIComponent(entityId)
            this.ensure("documentManagerService")
                .then(function (documentManagerService) {
                    return documentManagerService.getDocumentByEntityTypeWithId(entityType, entityId, documentId);
                })
                .then(function (document) {
                    self.downloadDocument(document, entityType, entityId, documentId);
                }).catch(function (error) {
                    var errorSummary = "";
                    if (error && error.responseJSON && error.responseJSON.message) {
                        errorSummary = error.responseJSON.message;
                    }
                    console.warn(errorSummary);
                });

        },

        downloadDocument: function (document, entity, entityId, documentId) {
            if(this.configuration.services.documentManager == undefined){
                throw new Error("documentManager service not found");
            }
            var baseUrl = this.configuration.services.documentManager;
            var url = [baseUrl, entity, entityId, documentId, "download"].join("/");
            var fileName = "";
            if (document != null)
                fileName = document.fileName || "";
            var x = new XMLHttpRequest();
            x.open("GET", url, true);
            x.responseType = "blob";
            x.setRequestHeader("Authorization", "Bearer " + this.$.session.get("token"));
            x.onload = function (e) {
                download(e.target.response, fileName);
            };
            x.send();
        },

        taxReturnsUpdated: function (customerInstitutions) {
            $("#divTaxReturnsUpload").hide();
            $("#divTaxReturnsGrid").hide();
            $(this.$$("#liTaxreturnsNew")).find('input:text').val('');
            $(this.querySelectorAll("#taxEditList")).hide();
            //$(this.querySelectorAll("#taxList")).show();

            var self = this;
            self.set("busy", false);
            self.set("disableClass", "");
            self.institutionsWithTaxReturns = customerInstitutions;
        },

        EditInfoTaxReturn: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-tax-" + id);
            $(liW2s).hide();
            var liW2S1 = this.$$(".li-li-tax-" + id);
            $(liW2S1).show();
            $(liW2S1).parent().parent().find("#liEditTaxInstitutionName").show();
            $(liW2S1).parent().parent().find("#liTaxInstitutionName").hide();
        },

        CancelInfoTaxReturn: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-tax-" + id);
            $(liW2s).show();
            var liW2S1 = this.$$(".li-li-tax-" + id);
            $(liW2S1).hide();
            $(liW2S1).parent().parent().find("#liEditTaxInstitutionName").hide();
            $(liW2S1).parent().parent().find("#liTaxInstitutionName").show();
            // this.fire("tax-returns-documents-remove");
        },

        CancelInfoNew: function (e) {
            $(this.$$("#liTaxreturnsNew")).find('input:text').val('');
           // this.fire("tax-returns-documents-remove");
        },

        taxClass: function (parent_index, child_index) {
            // return 'secndrow clearfix li-tax-' + parent_index + '-' + child_index;
            return 'row clearfix li-tax-' + parent_index + '-' + child_index;
        },

        taxClassEdit: function (parent_index, child_index) {
            // return 'secndrow clearfix div-margin li-li-tax-' + parent_index + '-' + child_index;
            return 'row clearfix div-margin li-li-tax-'  + parent_index + '-' + child_index;
        },

        displaySyncDiv: function (e) {
            if (e.target.getAttribute("disabled") === null) {
                $("#divTaxReturnsUpload").hide();
                $("#divTaxReturnsGrid").show();
            }
        },

        displayUploadDiv: function (e) {
            if (e.target.getAttribute("disabled") === null) {
                $("#divTaxReturnsGrid").hide();
                $("#divTaxReturnsUpload").show();

                if (e.target.dataset && e.target.dataset.id) {
                    var id = e.target.dataset.id;
                    var liW2s = this.$$(".li-tax-" + id);
                    $(liW2s).hide();
                    var liW2S1 = this.$$(".li-li-tax-" + id);
                    $(liW2S1).show();
                }
            }
        },

        getAsyncClass: function (value, state) {
            if (state == "not-connected") return "no-click";
            if (value) return "no-click"; else return "active";
        },

        getUploadClass: function (value, state) {
            if (state == "not-connected") return "no-click";
            if (value) return "active"; else return "no-click";
        },

        getStatusClass: function (value) {
            if (value == "connected") return "df df-tick"; else return "df df-alert";
        },

        handleNewSaveClick: function (e) {
            var liW2s = this.$$("#liTaxreturnsNew");
            var self = this;
            self.isValid(liW2s)
                .then(function (isValid) {
                    if (isValid) {
                        self.set("busy", true);
                        self.set("disableClass", "no-click");
                        var customerId = self.customerId;
                        self.taxReturnsSave(liW2s, "", customerId);
                    }
                }.bind(self));
        },

        handleTaxSaveClick: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-li-tax-" + id);
            var self = this;
            self.isValid(liW2s)
                .then(function (isValid) {
                    if (isValid) {
                        self.set("busy", true);
                        self.set("disableClass", "no-click");
                        var institutionId = e.target.dataset.institutionId;
                        var customerId = self.customerId;
                        self.taxReturnsSave(liW2s, institutionId, customerId);
                    }
                }.bind(self));
        },

        EditInfoPayStub: function (e) {
            var id = e.target.dataset.id;
            var liPayStub = this.$$(".li-tax-" + id);
            $(liPayStub).hide();
            var liPayStub1 = this.$$(".li-li-tax-" + id);
            $(liPayStub1).show();
        },

        CancelInfoPayStub: function (e) {
            var id = e.target.dataset.id;
            var liPayStub = this.$$(".li-tax-" + id);
            $(liPayStub).show();
            var liPayStub1 = this.$$(".li-li-tax-" + id);
            $(liPayStub1).hide();
            this.fire("tax-returns-documents-remove");
        },

        getValue: function (value) {
            if (value)
                return value;
            else return "";
        },

        getDateFormattedValue: function (yearDate) {
            var year = yearDate.split(' ')[1];
            return "01/01/" + year;
        },

        isValid: function (liW2s) {
            var self = this;
            //Validate All ui elements
            var _validations = [];

            liW2s.querySelectorAll('ui-container').forEach(function (_uiInputContainer) {
                _validations.push(_uiInputContainer.isValid());
            });

            //We do not need to chain validation here as they are not dependent on each other.
            return Promise.all(_validations)
                .then(function (validationResult) {
                    if (validationResult.indexOf(false) == -1) {
                        //Validation OK on the screen go ahead
                        return true;
                    } else {
                        //Validation Failed
                        return false;
                    }
                });
        },

        taxReturnsSave: function (liW2s, institutionId, customerId) {
            if(institutionId === "")
            {
                var taxInstitutionName = $(liW2s).find("#taxInstitutionName").val();
                var taxInstitutionOwner = $(liW2s).find("#taxInstitutionOwner").val();
                var taxType = $(liW2s).find("#taxInstitutionType").val();
            }
            else
            {
                var taxInstitutionName = $(liW2s).parent().parent().find("#liEditTaxInstitutionName").find("#editInstitutionName").val();
                var taxInstitutionOwner = $(liW2s).find("#taxInstitutionOwnerEdit").val();
                var taxType = $(liW2s).find("#taxInstitutionTypeEdit").val();
            }            

            var metadata = JSON.stringify({ 'Id': institutionId, 'Owner': taxInstitutionOwner, 'Institution': taxInstitutionName, 'Type': taxType });
            var payload = {};
            payload.metadata = metadata;
            if(this.configuration.services.capacityConnect == undefined){
                throw new Error("capacityConnect service not found");
            }
            payload.targetUrl = this.configuration.services.capacityConnect + "/customer/" + customerId + "/TaxReturn/manual/" + this.applicationNumber;
            payload.submitUrl = "/customer/" + customerId + "/TaxReturn/manual/" + this.applicationNumber;

            this.fire("tax-returns-save", payload);
        },

        handleNextClick: function (e) {
            $("#divTaxReturnsGrid").hide();
            $("#divTaxReturnsUpload").hide();
            $(this.$$("#liTaxreturnsNew")).find('input:text').val('');
            $(this.querySelectorAll("#taxEditList")).hide();
            //$(this.querySelectorAll("#taxList")).show();

            var self = this;
            self.set("busy", false);
            self.set("disableClass", "");

            this.fire("tax-returns-submit");
        },

        getFormattedAmount: function (amount) {
            if (amount)
                return '$' + amount;
            else return amount;
        },
        getFileId: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].fileId;
            else
                return "";
        },
        getDocumentId: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].documentId;
             else
                return "";
        },
        getFileName: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].fileName;
            else
                return "";
        },
        getDocuments: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].documents;
            else
                return "";
        },
        getName: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].name)
                return listOfDocuments[0].name;
            else return "";
        },
        getDocumentSubType: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].documentSubType)
               return listOfDocuments[0].documentSubType;
            else return "";
        }
    })
})();