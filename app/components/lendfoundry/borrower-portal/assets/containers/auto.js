(function () {
  /**
   * @class assetsContainer
   * @mixes alloy.lifeCycleEvents
   */
  var assetsContainer = {
    is: "assets-container",

    behaviors: [
      alloy.lifeCycleEvents,
      alloy.actions,
      alloy.visibilityController,
      ensureDependenciesBehavior
    ],

    properties: {
     
      /** @memberOf assetsContainer */
      manualFormData: {
        type: Object,
        value: function () {
          return {}
        }
      },
      busy: Boolean,
      configuration: Object,
      applicationNumber: String,
      deleteContext: {
        type: Object,
        value: function () {
          return {}
        }
      },

      /** @memberOf assetsContainer */
      customer: {
        type: Object,
        value: function () {
          return {}
        }
      },
      /** @memberOf assetsContainer */
      customerId: {
        type: String,
        value: ""
      },
      getAccessDetails:Object,
      firstName: {
        type: String,
        value: ""
      },

      /** @memberOf assetsContainer */
      providerId: {
        type: String,
        value: ""
      },

      /** @memberOf assetsContainer */
      institutions: {
        type: Array,
        value: function () {
          return []
        }
      },

      accountTransactions: {
        type: Array,
        value: function () {
          return []
        }
      },

      /** @memberOf assetsContainer */
      filterInstitutions: {
        type: Array,


        value: function () {
          return []
        }
      },


      /** @memberOf assetsContainer */
      customerAccounts: {
        type: Array,
        value: function () {
          return []
        }
      },
      customerInstitutions: {
        type: Array,
        value: function () {
          return []
        }
      },

      /** @memberOf assetsContainer */
      selectedInstitutionId: {
        type: String,
        value: ""
      },
      selectedPlaidItemId: {
        type: String,
        value: ""
      },
      isCredentialChanged: {
        type: Boolean,
        value: false
      },
      /** @memberOf assetsContainer */
      selectedLogo: {
        type: String,
        value: ""
      },

      /** @memberOf assetsContainer */
      selectedInstitution: {
        type: Object
      },

      /** @memberOf assetsContainer */
      challenges: {
        type: Array,
        value: function () {
          return []
        }
      },
      /** @memberOf assetsContainer */
      mfa: Object,
      institutionId: String,
      loggedInUsername: String,

      events: {
        type: Object,
        value: function () {
          /**
           * @name events
           * @memberOf assets-container
           */
          return {
            SELECT: "assets-select",
            MANUAL_ADD: "assets-manual-add",
            SUBMIT: "assets-submit",
            SET_VISIBILITY_MODE: "assets-set-visibility-mode",
            ANSWER: "assets-answer-challenge",
          }
        },
      },

      visibilityModes: {
        type: Object,
        value: function () {
          /**
           * @name visibilityModes
           * @memberOf assets-container
           */
          return {
            INIT: "INIT",
            ERROR: "ERROR",
            LOADING: "LOADING",
            FORM: "FORM",
            MANUAL: "MANUAL",
            GRID: "GRID",
            CHALLENGE: "CHALLENGE",
            CHALLENGE_ERROR: "CHALLENGE_ERROR",
            LIST: "LIST",
            CREDENTIAL_ERROR: "CREDENTIAL_ERROR",
            TRANSACTION: "TRANSACTION",
            VERIFYASSETS: "VERIFYASSETS"            
          }
        }
      },
      actionTypeModes: {
        type: Object,
        value: function () {
          return {
            PREAPPLICATION: "PREAPPLICATION",
            POSTAPPLICATION: "POSTAPPLICATION"
          }
        }
      },
      currentActionType: {
        type: String,
        value: "PREAPPLICATION"
      },

      /** @memberOf assetsContainer */
      activeVisibilityMode: {
        type: String,
        value: "INIT",
        observer: '_activeVisibilityModeChanged'
      },

      /** @memberOf assetsContainer */
      totalAssets: {
        type: String,
        value: "0",
        computed: "computeTotalAssets(customerAccounts)"
      },

      bankAlertInfo: {
        type: Object
      },

      /** @memberOf assetsContainer */
      error: {
        type: Object
      },
      /*Need to pass to DCC endpoints*/
      institutionType: {
        type: String,
        value: 'Banking'
      },
      /**
       * out-standing condition for which this component is loaded
       */
      outStandingCondition: Object,
      isSubmitVisible: Boolean,
      isNextVisible: Boolean,
      visibleButton: {
        type: Boolean,
        value: true
      },
      filterInstitutionsNotFound: {
        type: Boolean,
        value: false
      },
      refreshSummaryList: {
        type: Boolean,
        value: true
      },
      isRedirectedFromSummary: { type: Boolean, value: false },
      isEnterManually: { type: Boolean, value: true },
      isVerifyElectronically: { type: Boolean, value: true },
      loanPurpose:String
    },

    computeTotalAssets: function (accounts) {
      return _.reduce(accounts, function (sum, account) {
        sum += account.currentBalance;
        return sum
      }, 0)
    },

    observers: [
      "initValues(selectedValues)"
    ],

    require: [
      "dccService",
      "addUpdateDCCInstitution",
      "removeDCCDocumentOrAccount",
      "answerChallenge",
      "progressBar"
    ],

    listeners: {
      "filethis-fix-connection": "handleFileThisFixConnection",
      "assets-select": "handleSelect",
      "assets-manual-add": "handleManualAdd",
      "assets-submit": "handleSubmit",
      "assets-remove": "handleRemove",
      "assets-answer-challenge": "handleAnswerChallenge",
      "assets-set-visibility-mode": "handleSetVisibilityMode",
      "assets-navigate": "handleSetVisibilityMode",
      "assets-done": "handleDone",
      "assets-plaid-ui-link" : "handlePlaid",
      "assets-async": "handleAsync",
      "filethis-assets-async": "handleFileThisAsync",
      "post-assets-submit": "handlePostAssetsSubmit",
      "assets-documents-remove": "handleAssetsDocumentsRemove",
      "post-assets-save": "handleAssetsSave",
      "w2s-and-paystubs-refresh": "handleAssetsRefresh",
      "show-transactions": "handleTransactions",
      "confirm": "executeDelete",
      "on-load-plaid":"onLoadPlaid",
      "on-success-plaid":"onSuccessPlaid",
      "on-exit-plaid":"onExitPlaid",
    },

    openConfirmModal: function (context) {
      this.deleteContext = context;
      this.$.confirm.open();
    },

    /*Confirm delete*/
    executeDelete: function () {
      var self = this;

      if (_.isUndefined(self.deleteContext.accountDocumentId))
        return;

      self.$.removeDCCDocumentOrAccount.execute(self.deleteContext)
        .then(function (result) {
          if (!_.isNull(result)) {
            self.showList();
            self.deleteContext.accountDocumentId = undefined;
          }
        })
        .catch(function (error) { self.handleError(error) });
    },

    attached: function () {
      var self = this;
      this.applicationNumber = this.$.urlParser.getParams().id;
      this.set("loanPurpose",this.getLoanPurpose());
      this.set("activeVisibilityMode", this.currentActionType === "PREAPPLICATION" && !this.isRedirectedFromSummary ? "VERIFYASSETS" : "LOADING");

      // Wrap with new functionality:
      // Ensure the progress bar finishes before being hidden
      // Ensure the progress bar is running before being shown
      this.changeVisibilityMode =
        _.wrap(this.changeVisibilityMode.bind(this), function (changeVisibilityMode, nextVisibilityMode) {
          var LOADING = this.visibilityModes.LOADING;

          if (nextVisibilityMode === LOADING) {
            this.$.progressBar.start();
          }

          if (this.activeVisibilityMode === LOADING) {
            return this.$.progressBar.finish().then(function () {
              changeVisibilityMode(nextVisibilityMode)
            })
          }

          changeVisibilityMode(nextVisibilityMode)
        }.bind(this))
        self.ensure('dccService').then(function (dccService) {
          dccService.plaidConfiguration("Banking").then(function (response){
            self.set("getAccessDetails", []);
            self.set("getAccessDetails", response);
            });
        });
      this.init()
    },
    /**
             * Replace place holder loan purpose to display
             */
            getLoanPurpose: function () {
             var _placeHolder ="<#loanpurpose#>";
             var placeHolders = this.assetsPlaceHolders;
             var _placeHolderValue = "";
             if(placeHolders){
             if (placeHolders[_placeHolder] != undefined && placeHolders[_placeHolder] != null
              && placeHolders[_placeHolder] != '') {
                _placeHolderValue= placeHolders[_placeHolder];
              }
          }
          return _placeHolderValue;
          },
    fetchCustomerInstitutions: function (customerId, applicationNumber=self.applicationNumber) {
      return this.$.dccService.fetchCustomerInstitutions(customerId, applicationNumber);
    },
    fetchCustomerInstitutionsSuccess: function (result) {
      var self = this

      self.customerAccounts = result.customerAccounts;
      self.institutions = result.institutions;
      self.customerInstitutions = result.customerInstitutions;
      self.customerAccountsByInstitution = result.customerAccountsByInstitution;
      self.customerAccountsListData = _.values(self.customerAccountsByInstitution);
    },
    getSavingsTitle: function () {
      return 'Please tell us about your assets.';
    },
    getManualSavingsTitle: function () {
      return 'Next we’ll take a look at your savings and investments.';
    },
    getConsentTitle: function () {
      return 'How would you like to verify your assets? You can either manually enter details for each institution and upload your documents by clicking “Enter Manually”, or you can provide login details to sync your accounts with the system by clicking “Verify Electronically”. Please select an option below.';
    },
    getBankLoginTitle: function () {
      return 'Please enter login details to sync your accounts with the system.';
    },
    getBankTitle: function () {
      return 'Below are the financial institutions that you have provided with your application.';
    },

    handleAssetsDocumentsRemove: function (event) {
      if (!_.isNull(this.$$("#manualDocument")))
        $(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').removeAllDocuments();
    },
    handleAssetsRefresh: function (event) {
      var self = this;
      self.init();
    },
    handleFileThisFixConnection: function (event) {
      var self = this;
      var state = _.get(event, "detail.actionState");

      if (state === "connected")
        return;

      self.selectedInstitutionId = _.get(event, "detail.actionInstitutionId");
      self.institutionId = _.get(event, "detail.actionInstitutionId");
      self.customerId = _.get(event, "detail.actionApplicantId");
      self.accountId = _.get(event, "detail.actionAccountId");
      self.connectionId = _.get(event, "detail.actionConnectionId");

      self.ensure("dccService")
        .then(function (service) {
          /**
           * Check State
           */
          service
            .checkConnectionState(self.customerId, self.accountId, self.connectionId)
            .then(function (response) {
              if (!_.isUndefined(response)) {
                if (response.state === "question") {
                  service.getChallengeQuqestion(
                    self.customerId,
                    self.selectedInstitutionId,
                    self.accountId,
                    self.connectionId,
                    self.applicationNumber)
                    .then(function (response) {
                      if (!_.isUndefined(response.mfa)) {
                        self.logo = response.logo;
                        self.mfa = response.mfa;
                        self.interactionId = response.interactionId;
                        return self.showChallenge();
                      }
                    });
                }
                else {
                  self.showLoading();
                  self.init();
                }
              }
            });
        });
    },
    handleAssetsSave: function (event) {
      var self = this;
      if (event.detail) {
        if ($(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').filesAdded) {
          var payload = {};
          payload.metadata = event.detail.metadata;
          payload.targetUrl = event.detail.targetUrl;
          this.set("payload", payload);
          $(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').processQueue(payload);
        }
        else {
          var payloadForm = new FormData();
          payloadForm.append('metadata', event.detail.metadata);
          payloadForm.append('files', '');
          if(this.configuration.services.capacityConnect == undefined){
            throw new Error("capacityConnect service not found");
          }
          this.ensure("customHttpRequestService")
            .then(function (service) {
              return service.post(event.detail.submitUrl, payloadForm, self.configuration.services.capacityConnect);
            })
            .then(function (response) {
              self.init();
            })
            .catch(function (error) {
              console.warn(error);
            });
        }
      }
    },
    handlePlaid:function(){
      var self = this;
      self.set("busy", true);
      var handler = Plaid.create({
        clientName: self.getAccessDetails.plaidUIHeaderText,
        env: self.getAccessDetails.plaidEnvironment,
        key: self.getAccessDetails.publicKeys,
        product: self.getAccessDetails.initialProduct,
        webhook: self.getAccessDetails.webhookUrl,
        onLoad: function() {
          self.fire('on-load-plaid');
        },
        onSuccess: function(public_token, metadata) {
          var payload={
            metadata:metadata
            };
          self.fire('on-success-plaid', payload);
        },
        onExit: function(err, metadata) {
          var payload={
            err:err,
            metadata:metadata
            };
          self.fire('on-exit-plaid', payload);
        },
      });
      handler.open();
    },
    onLoadPlaid: function() {
      var self = this;
      self.set("busy", false);
      },
    onSuccessPlaid: function(payloadToken) {
      var self = this;
      var institution_id = payloadToken.detail.metadata.institution.institution_id;
      var payload = payloadToken.detail.metadata;
      var metadata =JSON.stringify(payload);
      self.ensure("dccService")
      .then(function (service) {
        self.showLoading();
       service.addPlaidInstitution(self.customerId,institution_id,metadata,undefined,self.applicationNumber).then(function (response){
        if (_.isNull(response))
        return;
        self.set("activeVisibilityMode", "INIT");
              self.fire(self.events.SET_VISIBILITY_MODE, {
                visibilityMode: self.visibilityModes.LIST
         })
        self.fire("required-condition-adjust-width", 90);
      });
    });
    
    },
    onExitPlaid: function(payloadexit) {
      if (payloadexit.detail.err != null) {
      }
    },
    /**

     * Update width of parent as per visibility mode
     */
    _activeVisibilityModeChanged: function (newValue, oldValue) {
      if (!this.isPreApplication(self.currentActionType, self.actionTypeModes)) {
        var widthToChange;
        switch (newValue) {
          case this.visibilityModes.LIST:
          case this.visibilityModes.GRID:
            widthToChange = 90;
            break;
          default:
            widthToChange = 90;
        }
        this.fire("required-condition-adjust-width", widthToChange);

      }
    },


    refreshGrid: function () {


      this.showLoading();
      this.$.progressBar.start()
      this.init();

    },
    showAddAccountButton :function(enterManual,verifyElectronically){
      if(enterManual && !verifyElectronically)
      {
         return true;
      }              
      return false;
    },
    isPreApplication: function (currentActionType, actionTypeModes) {

      if (!_.isUndefined(currentActionType) && !_.isUndefined(actionTypeModes))
        return currentActionType === actionTypeModes.PREAPPLICATION;
    },
    computeStyleClasses: function (currentActionType, actionTypeModes) {

      if (this.isPreApplication(currentActionType, actionTypeModes))
        return ["col-lg-6", "col-md-10", "col-sm-12", "col-xs-12", "wrapper-narrow", "no-pad"].join(" ");
      else
        return "";
    },
    handleSelect: function (event) {
      var self = this;

      var id = _.get(event, "detail.actionInstitutionId")
      if (!id)
        return console.warn("Select called with invalid institutionsId of " + id)

      self.ensure("dccService")
        .then(function (service) {
          service.fetchInstitutionDetail(id).then(function (response) {
            self.selectedInstitution = response;
            self.selectedInstitutionId = id;

            $(self.$$("#divAssetsGrid")).hide();
            $(self.$$("#divAssetsUpload")).hide();

            self.fire(self.events.SET_VISIBILITY_MODE, {
              visibilityMode: self.visibilityModes.FORM
            })
          });
        });
    },
    handlePostAssetsSubmit: function (event) {
      var self = this;
      self.inProgress = true;
      self.ensure("dccService")
        .then(function (service) {
          service.submitOutStandingCondition(self.outStandingCondition.id, self.applicationNumber, self.customerId).then(function () {
            self.moveToNextCondition('completed');
            self.inProgress = false;
          });
        });
    },
    /**
     * This will raise an action (done/skipped) to move to next condition
     */
    moveToNextCondition: function (actionName) {
      var payload = {
        actionName: actionName,
        currentSelectedIndex: this.outStandingCondition.index,
        requestId: this.outStandingCondition.id
      };
      this.fire("required-condition-completed", payload);

      this.fire("required-condition-adjust-width", 90);
    },
    handleAsync: function (event) {
      var self = this;
      var id = _.get(event, "detail.actionInstitutionId")
      var accountId = _.get(event, "detail.actionAccountId")
      if (!id)
        return console.warn("Select called with invalid institutionsId of " + id)
      if (!accountId)
        return console.warn("Unable to remove asset without accountId")

      self.institutionType = "Banking";

      self.ensure("dccService")
        .then(function (service) {
          service.fetchAsyncAccount(id, accountId, self.customerId, self.institutionType, self.applicationNumber)
             .then(function (asyncResponse) {
            if (asyncResponse != null) {
              self.isCredentialChanged = asyncResponse.isCredentialsChanged

              if (!_.isUndefined(asyncResponse.error) && asyncResponse.error !== null && !_.isUndefined(asyncResponse.error.error_Message) && asyncResponse.error.error_Message !== null)
                self.handleError(asyncResponse.error.error_Message);

              if (self.isCredentialChanged) {
                self.selectedPlaidItemId = asyncResponse.plaid_Item_Id;
                service.fetchInstitutionDetail(id, self.institutionType).then(function (response) {
                  self.selectedInstitution = response;
                  self.selectedInstitutionId = id;
                  self.fire(self.events.SET_VISIBILITY_MODE, {
                    visibilityMode: self.visibilityModes.FORM
                  })
                });
              }
              else
                return self.showList();
            }
          });
        });
    },
    handleFileThisAsync: function (event) {
      var self = this;
      var institutionId = _.get(event, "detail.actionInstitutionId");
      var applicantId = _.get(event, "detail.actionApplicantId");

      self.ensure("dccService")
        .then(function (service) {
          service.w2sandPaystubSync(applicantId, institutionId, self.applicationNumber)
            .then(function () {
              self.showLoading();
              self.init();
            });
        });

    },
    handleManualAdd: function () {
      var self = this;
      var form = self.$$("#manualAdd").$.form

      form.isValid()
        .then(function (isValid) {
          if (!isValid) return

          return self.ensure("dccService")
            .then(function (svc) {
              /** @type {DCC.Service} */
              var service = svc
              
              self.showLoading();

              return service.addBankDetail(
                self.customerId,
                self.manualFormData.institutionName,
                self.manualFormData.accountType,
                self.manualFormData.accountNumber,
                self.scrubMoneyData(self.manualFormData.currentBalance),
                self.manualFormData.accountHolder,
                self.applicationNumber
                )

            })
            .then(function () {
              form.setData({})
              self.set("activeVisibilityMode", "INIT");
              self.fire(self.events.SET_VISIBILITY_MODE, {
                visibilityMode: self.visibilityModes.LIST
              })
              self.fire("required-condition-adjust-width", 90);
              
            });
        })
    },



    handleSetVisibilityMode: function (event) {
      var self = this
      /** @type {string} */
      /** @type {string} */
      var nextVisibilityMode;
      nextVisibilityMode =
        _.get(event, "detail.actionVisibilityMode") ||
        _.get(event, "detail.visibilityMode");
      var GRID = self.visibilityModes.GRID
      var LIST = self.visibilityModes.LIST;

      var leftNavigationNext = _.get(event, "detail.actionNext");
      var leftNavigationBack = _.get(event, "detail.actionBack");

      var skippable = _.get(event, "detail.actionSkippable") || false;
      // Ensure progress bar is started after it is visible

      if (leftNavigationNext) {
        this.$.hub.publish("section:next", {
          sectionNext: true
        });
      }
      if (leftNavigationBack) {
        this.$.hub.publish("section:back", {
          sectionBack: true
        });
      }

      if (nextVisibilityMode === GRID && !self.isPreApplication(self.currentActionType, self.actionTypeModes))
        nextVisibilityMode = LIST;

      // Fetch institutions before showing grid
      if (nextVisibilityMode === GRID) {
        return self.fetchInstitutions()
          .then(function (institutions) {
            self.institutions = institutions

            self.changeVisibilityMode(nextVisibilityMode)
          })
          .catch(self.handleError);
      }

       //we have to display back button on manual screen only when nextVisisbilitymode is manual
      //isManualAssetFlow=true configured and isVerifiedAssetFlow is false and total asset count is > 0
      if(nextVisibilityMode === self.visibilityModes.MANUAL && self.isEnterManually )//&& !self.isVerifyElectronically)
      {
         this.$.dccService.fetchCustomerAccounts(self.customerId, self.applicationNumber)
         .then(function (responses) {
          var customerAccountsRaw = responses;

          self.assets = responses;
        })
        .catch(self.handleError)
      }


      // Fetch combined institutions and bankAlertsInfo before showing list
      if (nextVisibilityMode === LIST) {

        self.fire("required-condition-adjust-width", 90);

        var loadAccounts = this.$.dccService.fetchCustomerAccounts(self.customerId, self.applicationNumber);


        if (self.isPreApplication(self.currentActionType, self.actionTypeModes)) {
          var loadBankAlertInfo = self.$.questionnaire.fetchBankAlertInfo(self.applicationNumber, self.customerId);
        }

        return Promise.all([loadAccounts, loadBankAlertInfo])
          .then(function (responses) {


            var customerAccountsRaw = responses[0];

            self.assets = responses[0];
            if(!skippable){
            if (self.assets && self.assets.length === 0 && self.activeVisibilityMode != self.visibilityModes.LIST) {
              if (!self.isPreApplication(self.currentActionType, self.actionTypeModes)) {
                return self.changeVisibilityMode(self.visibilityModes.LIST)
              }
              else if (self.isEnterManually && !self.isVerifyElectronically) {
                return self.changeVisibilityMode(self.visibilityModes.MANUAL)
              }
              else {
                self.$.hub.publish("subSection:count", {
                  subSectionCount: 3,
                  currentSubSection: 1
                });
                return self.changeVisibilityMode(self.visibilityModes.VERIFYASSETS);
              }
            }
          }
            if (responses[1]) {
              var loadBankAlertInfo = responses[1];
              self.bankAlertInfo = loadBankAlertInfo;
            }

            if (nextVisibilityMode === LIST && self.activeVisibilityMode != LIST) {
              self.$.hub.publish("section:next", {
                sectionNext: true
              });
            }

            self.changeVisibilityMode(nextVisibilityMode)
          })
          .catch(self.handleError)
      }

      if (nextVisibilityMode === LIST && self.activeVisibilityMode != LIST) {
        self.$.hub.publish("section:next", {
          sectionNext: true
        });
      }
      self.changeVisibilityMode(nextVisibilityMode)
    },
    handleFilterInstitutions: function (filter) {
      var self = this;



      return self.searchInstitutions(filter).then(function (searchResponse) {
        self.set("filterInstitutions", []);
        self.set("filterInstitutions", searchResponse);
        if(searchResponse==null || searchResponse.length<=0){
          self.set("filterInstitutionsNotFound",true);
        }
        else{
          self.set("filterInstitutionsNotFound",false);
        }

        return searchResponse;
      });
    },

    handleTransactions: function (event) {
      var self = this
      var accountId = _.get(event, "detail.actionAccountId")

      if (!accountId)
        return console.warn("Unable to show transactions without accountId")

      self.ensure("dccService")
        .then(function (service) {
          service.fetchAccountTransactions(accountId, self.customerId, self.applicationNumber).then(function (response) {
            self.accountTransactions = response;
            self.showAccountTransactions();
          })
        });

    },

    handleSubmit: function (event) {
      var self = this
      var form = $("assets-forms-credentials")[0];

      return form.isValid()
        .then(function (isValid) {
          if (!isValid) return
          var formData = form.getFormData();
          var password = "";
          var username = "";
          var pin = "";
          var loggedinUsername = _.get(event, "detail.actionLoggedinUsername");
          if (formData != null) {
            password = formData.password || "";
            username = formData.username || "";
            pin = formData.pin || "";
          }
          form.reset()
          self.showLoading()

          /** @type {Interactors.AddUpdateDCCInstitution} */
          var addUpdateDCCInstitution = self.$.addUpdateDCCInstitution
          addUpdateDCCInstitution.institutionType = self.institutionType;

          /** @type {Interactors.AddUpdateDCCInstitutionContext} */
          var context
          // ToDO: Refactor the context payload with dynamically once back-end has to complete their end.
          context = {
            customerId: self.customerId,
            institutionId: self.selectedInstitutionId,
            password: password,
            username: username,
            pin: pin,
            plaidItemId: self.selectedPlaidItemId,
            applicationNumber: self.applicationNumber
          }

          addUpdateDCCInstitution.execute(context)
            .then(function (result) {
              if (_.isNull(result))
                return;

              if (!result.isSuccess) {
                if (self.isPreApplication(self.currentActionType, self.actionTypeModes) || self.institutionType === "Banking") {
                  if (!_.isUndefined(result.error) && !_.isUndefined(result.error.display_Message)) {
                    self.handleError(result.error.display_Message);
                    self.set("activeVisibilityMode", "INIT");
                    return self.showCredentialError()
                  }
                  else if (!_.isUndefined(result.mfa)) {
                    self.mfa = result.mfa;
                    self.institutionId = result.institutionId;
                    self.set("activeVisibilityMode", "INIT");
                    return self.showChallenge();
                  }
                }
                else {
                  self.accountId = result.fileThisAccountId;
                  self.connectionId = result.connectionId;
                  var timeCounter = 0;
                  var interval = setInterval(function () {
                    self.ensure("dccService")
                      .then(function (service) {
                        service.checkConnectionState(self.customerId, self.accountId, self.connectionId, self.applicationNumber)
                        .then(function (response) {
                          if (!_.isUndefined(response)) {
                            var state = response.state;
                            timeCounter = timeCounter + 1;
                            if (state === "question") {
                              clearInterval(interval);
                              service.getChallengeQuqestion(
                                self.customerId, 
                                self.selectedInstitutionId, 
                                self.accountId, 
                                self.connectionId,
                                self.applicationNumber).then(function (response) {
                                if (!_.isUndefined(response.mfa)) {
                                  self.mfa = response.mfa;
                                  self.interactionId = response.interactionId;
                                  // self.institutionId = response.institutionId;
                                  return self.showChallenge();
                                }
                              });
                            }
                            if (state === "completed" || state === "uploading" || state === "waiting") {
                                clearInterval(interval);
                              //
                              self.showLoading();
                              // self.$$("#challengeForm").resetForm();

                              //TODO: Required Changes
                              setTimeout(() => {
                                self.fetchCustomerInstitutions(self.customerId)
                                .then(function (customerInstitutions) {
                                  self.customerInstitutions = customerInstitutions;
                                  self.fetchCustomerInstitutionsSuccess.bind(self)
                                })
                                .then(function () {
                                  self.showList();
                                });
                              }, 3000);
                            }
                            if (timeCounter >= 12) {
                              clearInterval(interval);
                              timeCounter = 0;
                              return self.showList();
                            }
                          }
                        });
                      });
                  }, 5000);
                }
              }
              else {
                self.set("activeVisibilityMode", "INIT");
                return self.showList();
              }
            })
            .catch(function (error) {
              self.set("activeVisibilityMode", "INIT");
              self.fire(self.events.SET_VISIBILITY_MODE, {
                visibilityMode: self.visibilityModes.FORM
              })
              self.handleError(error)
            })
        })
    },
    handleAnswerChallenge: function (event) {
      var self = this

      if (self.isPreApplication(self.currentActionType, self.actionTypeModes) || self.institutionType === "Banking") {
        var publicToken = _.get(event, "detail.actionChallengePublicToken");
        var mfaType = _.get(event, "detail.actionChallengeMfaType");
        var institutionId = _.get(event, "detail.actionChallengeInstitutionId");
        var responses = _.get(event, "detail.actionChallengeSelectedAnswer");
        responses = responses.split(",");


        var challenge = {
          "customerId": self.customerId,
          "publicToken": publicToken,
          "mfaType": mfaType,
          "institutionId": institutionId,
          "responses": responses
        }

        this.$.answerChallenge.institutionType = self.institutionType;
        this.$.answerChallenge.applicationNumber = self.applicationNumber;
        
        self.showLoading();
        this.$.answerChallenge.execute({
          challenge: challenge
        })
          .then(function (result) {
            if (!_.isNull(result)) {

              if (!result.isSuccess) {

                if (!_.isUndefined(result.error) && !_.isUndefined(result.error.display_Message)) {
                  self.set("activeVisibilityMode", "INIT");
                  return self.showChallengeError();

                } else if (!_.isUndefined(result.mfa)) {
                  self.mfa = result.mfa;
                  self.institutionId = result.institutionId;
                  self.set("activeVisibilityMode", "INIT");
                  return self.showChallenge();
                }
              } else {
                self.set("activeVisibilityMode", "INIT");
                return self.showList();
              }
            }
          })
      }
      else {
        self.showLoading()

        var responseIds = _.get(event, "detail.actionChallengeSelectedAnswerId");
        var responses = _.get(event, "detail.actionChallengeSelectedAnswer");
        responseIds = responseIds.split(",");
        responses = responses.split(",");

        var challenge = {
          "customerId": self.customerId,
          "accountId": self.accountId,
          "connectionId": self.connectionId,
          "interactionId": self.interactionId,
          "responseIds": responseIds,
          "responses": responses
        }

        this.$.answerChallengeFileThis.execute({
          challenge: challenge
        }).then(function (result) {
          var timeCounter = 0;

          setTimeout(function () {
            var interval = setInterval(function () {
              self.ensure("dccService")
                .then(function (service) {
                  service.checkConnectionState(self.customerId, self.accountId, self.connectionId).then(function (response) {
                    if (!_.isUndefined(response)) {
                      var state = response.state;
                      timeCounter = timeCounter + 1;
                      if (state === "question") {
                        clearInterval(interval);
                        service.getChallengeQuqestion(
                            self.customerId, 
                            self.selectedInstitutionId, 
                            self.accountId, 
                            self.connectionId,
                            self.applicationNumber)
                          .then(function (response) {
                          if (!_.isUndefined(response.mfa)) {
                            self.mfa = response.mfa;
                            self.interactionId = response.interactionId;
                            // self.institutionId = response.institutionId;
                            return self.showChallenge();
                          }
                        });
                      }
                      if (state === "completed" || state === "uploading" || state === "waiting") {
                        clearInterval(interval);
                        //
                        self.showLoading();
                        //self.$$("#challengeForm").resetForm();

                        //TODO: Required Changes
                        setTimeout(() => {
                          self.fetchCustomerInstitutions(self.customerId)
                          .then(function (customerInstitutions) {
                            self.customerInstitutions = customerInstitutions;
                            self.fetchCustomerInstitutionsSuccess.bind(self)
                          })
                          .then(function () {
                            self.showList();
                          });
                        }, 3000);
                      }
                      if (timeCounter >= 12) {
                        clearInterval(interval);
                        timeCounter = 0;
                        return self.showList();
                      }
                    }
                  });
                });
            }, 5000);
          }, 10000);
        })
          .catch(function (error) {
            self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.FORM })
            self.handleError(error)
          })
      }
    },
    handleRemove: function (event) {
      var self = this
      var accountId = _.get(event, "detail.actionAccountId")
      var type = _.get(event, "detail.actionType")

      if (!accountId)
        return console.warn("Unable to remove asset without accountId")


      /** @type {Interactors.removeDCCDocumentOrAccount} */
      var removeDCCDocumentOrAccount = self.$.removeDCCDocumentOrAccount;
      removeDCCDocumentOrAccount.institutionType = "Banking";
      if (type) {
        removeDCCDocumentOrAccount.institutionType = "FileThisBanking";
      }

      var context;
      context = {
        accountDocumentId: accountId,
        customerId: self.customerId,
        entityType: type
      }

      this.openConfirmModal(context);
    },

    handleDone: function () {
      var actionType = "submit";
      if (this.isNextVisible)
        actionType = "next";
      window._eventHub.publish("section:action", {
        actionType: actionType
      });
    },
    /**
     * Prepares dcc service for use
     * @returns {Promise<*>}
     */
    initDcc: function () {
      return this.ensure("initDcc")
        .then(function (initDcc) {
          return initDcc.execute()
        })
    },

    /**
     * Prepare questionnaire credentials for use
     * @returns {Promise<*>}
     */
    initQuestionnaire: function () {
      var self = this;

      if (self.applicationNumber)
        return Promise.resolve(self.applicationNumber)
       
      return self.fetchTemporaryApplicationNumber()
        .then(function (applicationNumber) {
          self.set("applicationNumber", applicationNumber);
          return applicationNumber
        })
    },
    fetchApplicants: function() {
      var self = this;
      return self.ensure("questionnaire")
      .then(function (questionnaire) {
        return questionnaire.fetchApplicantWithSpouseName(self.applicationNumber)
        .then(function (applicants) {
            var loggedInApplicant = applicants.filter(function (memberItem) {
              return memberItem.userName === self.loggedInUsername;
            });
            if(loggedInApplicant.length > 0){
              if(loggedInApplicant[0].actualApplicantType === "Spouse" || loggedInApplicant[0].actualApplicantType === "CoBorrowerSpouse")
                self.customerId = loggedInApplicant[0].spouseUserName;
            }
        })                                 
      })
    },
    /**
     * Performs intialization chores. Stores customer and application number and sets visibility mode
     */
    preAssetInit:function(){
      var self = this;
      self.$.hub.publish("subSection:count", {
        subSectionCount: 3,
        currentSubSection: 1
      });
    Promise.all([
      this.initQuestionnaire(),
      this.initDcc()
    ])
      .then(function (responses) {
        self.applicationNumber = responses[0];
        self.customer = responses[1].customer;

        self.loggedInUsername = responses[1].username;
        if (!self.customerId)
          self.customerId = self.loggedInUsername;          
      })      
      .then(function () {
        
          if (self.isRedirectedFromSummary)
            self.fire(self.events.SET_VISIBILITY_MODE, {
              // visibilityMode: self.visibilityModes.GRID
              visibilityMode: self.visibilityModes.LIST

            });
          else {
            self.$.dccService.fetchCustomerAccounts(self.customerId, self.applicationNumber).then(function (responses) {
              if (self.isEnterManually && !self.isVerifyElectronically) {
                if(responses && responses.length > 0){
                  self.fire(self.events.SET_VISIBILITY_MODE, {
                    //visibilityMode: self.visibilityModes.VERIFYASSETS
                    visibilityMode: self.visibilityModes.LIST
    
                  });
                }
                else{
                  self.fire(self.events.SET_VISIBILITY_MODE, {
                    // visibilityMode: self.visibilityModes.GRID
                    visibilityMode: self.visibilityModes.MANUAL
    
                  });
                }
               }
               else {
                self.fire(self.events.SET_VISIBILITY_MODE, {
                  //visibilityMode: self.visibilityModes.VERIFYASSETS
                  visibilityMode: self.visibilityModes.LIST
  
                })
              }
            })
            .catch(self.handleError)
          }
      })
      .catch(function (error) {
        //console.error(error)
      })
   },
   postAssetInit:function(){
     var self = this;
    Promise.all([   
      this.initDcc()
    ])
      .then(function (responses) {
        self.customer = responses[0].customer;
        self.loggedInUsername = responses[0].username;
        if (!self.customerId)
          self.customerId = self.loggedInUsername;
      })
      .then(function () {
        return self.fetchApplicants()
      })
      .then(function () {
        return self.fetchInstitutions()
      })
      .then(function (institutions) {

        self.institutions = institutions
      })
      .then(function () {
        return self.searchInstitutions("")
      })
      .then(function (searchResponse) {

        self.set("filterInstitutions", []);
        self.set("filterInstitutions", searchResponse);

      })
      .then(function () {
       
          self.fire(self.events.SET_VISIBILITY_MODE, {
            visibilityMode: self.visibilityModes.LIST
          })
          self.fire("required-condition-adjust-width", 90);

        
      })
      .catch(function (error) {
        console.error(error)
      })
   },
    init: function () {
      var self = this;
      self.set("activeVisibilityMode", "INIT");
      self.publish("sub-section:width", {
        width: 100
      });
      if (self.configuration.apps['pre-assets']) {
        self.set('isEnterManually', self.configuration.apps['pre-assets']['enter-manually']);
        self.set('isVerifyElectronically', self.configuration.apps['pre-assets']['verify-electronically']);
      }
     if(self.currentActionType === "PREAPPLICATION")
      self.preAssetInit();
      else
      self.postAssetInit();
     
    },
    fetchCurrentUser: function () {
      if(this.configuration.services.security == undefined){
        throw new Error("security service not found");
       }
      return this.$.http.get("/", this.configuration.services.security);
    },
    /**
     * Used to manually add a service via the questionnaire service and retrieve new bank alerts
     * @param {Object} data
     * @param {string} data.bankName
     * @param {string} data.institutionName
     * @param {string} data.accountType
     * @param {string} data.accountHolderName
     * @param {number} data.currentBalance
     * @param {string} data.accountNumber
     * @returns {Promise<*>}
     */
    addInstitution: function (data) {
      var self = this;
      var payload = {
        bankName: data.institutionName,
        accountType: data.accountType,
        accountHolderName: data.accountHolderName,
        currentBalance: self.scrubMoneyData(data.currentBalance),
        accountNumber: data.accountNumber
      };

      return this.ensure("questionnaire")
        .then(function (service) {
          return this.fetchCurrentUser().then(function (currentUserResponse) {
            return service.addInstitution(self.applicationNumber, payload)
              .then(function () {

                return service.fetchBankAlertInfo(self.applicationNumber, currentUserResponse.username)
              })
              .then(function (bankAlertInfo) {
                self.bankAlertInfo = bankAlertInfo;
                self.set("activeVisibilityMode", "INIT");
              })
          })
        })

    },
    /**
     * Managing assets require a temporary application number associated with the current user
     */
    fetchTemporaryApplicationNumber: function () {
      return this.ensure("questionnaire").then(function (service) {
        return service.createNewQuestionnaire()
      }).then(function (response) {
        return response.temporaryApplicationNumber;
      }).catch(function (error) {
        console.error(error);
      });
    },

    /**
     * Retrieve master list of available institutions
     * @returns {Promise<Array<object>>}
     */
    fetchInstitutions: function () {
      return this.$.dccService.fetchInstitutions()
    },
    /**
     * Retrieve search list of available institutions
     * @returns {Promise<Array<object>>}
     */

    searchInstitutions: function (filter) {
      return this.$.dccService.searchInstitutions(filter)
    },
    /**
     * This function will be called when we recieved selectedValues from parent
     */
    initValues: function (selectedValues) {
      if (selectedValues) {
        if (typeof selectedValues === "string") {
          selectedValues = JSON.parse(selectedValues);
        }

        selectedValues.map(function () {

        }, this);
      }
    },

    handleError: function (error) {
      if (!_.isUndefined(error))
          console.error(error)
    },

    getLogoBySelectionId: function (selectedInstitutionId) {
      return _(this.institutions).filter({
        institutionId: selectedInstitutionId
      }).get("0.logo")
    },

    /**
     * Clean data for endpoint
     * @param value
     * @returns {string}
     */
    scrubMoneyData: function (value) {
      return value.replace(/[^0-9.]/g, "");
    },

    showChallenge: function () {
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.CHALLENGE
      })
    },

    showChallengeError: function () {
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.CHALLENGE_ERROR
      })
    },
    showAccountTransactions: function () {
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.TRANSACTION
      })
    },
    showCredentialError: function () {
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.CREDENTIAL_ERROR
      })
    },
    showLoading: function () {
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.LOADING
      })
    },
    showListWithSyncGrid: function () {
      $("#divAssetsGrid").show();
      $("#divAssetsUpload").hide();
    },
    showList: function () {
      var self = this;
      this.fire(this.events.SET_VISIBILITY_MODE, {
        visibilityMode: this.visibilityModes.LIST
      })
      this.fire("required-condition-adjust-width", 90);
      self.set("activeVisibilityMode",self.visibilityModes.LIST);
    }
  }
  Polymer(assetsContainer)
})()