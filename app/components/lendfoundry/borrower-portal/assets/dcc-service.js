/**
 * This is the original and partially complete version of dcc service
 */

/**
 * @namespace DCC
 */

(function (Polymer) {
  var assert = {
    ok: function (value, message) {
      this.equal(value, true, message)
    },
    equal: function (value, expected, message) {
      if (value != expected) this._fail(value, expected, message)
    },
    _fail: function (value, expected, message) {
      var errorMessage = message || "Expected " + value + " to be " + expected;
      throw new Error(errorMessage)
    }   
  }

  var scope = {
    get token() {
      return this._token
    },

    set token(value) {
      this._token = value;
    },

    get username() {
      return this._username
    },

    set username(value) {
      this._username = value;
    },
    get institutionType() {
      return this._institutionType
    },

    set institutionType(value) {
      this._institutionType = value;
    },
    // institutionType: "banking",

    NOT_FOUND_STATUS: 404
  };

  var defaultConfig = {
    contentType: "application/json",
    beforeSend: function (req) {
      req.setRequestHeader("authorization", "Bearer " + scope.docittToken)
      req.setRequestHeader("X-Client-IP", scope.ipAddress)
    }
  };

  var methods = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE"
  };

  // var apiBaseUrl = this.configuration.services.capacityConnect;

  /**

   * DCC Service endpoint URLs
   * @type {{url: url, login: string, customer: string, institutions: string, addInstitution:
   *    addInstitution, customerAccounts: customerAccounts,  answerChallenge: answerChallenge,
   *    fetchAsyncAccount:fetchAsyncAccount, updateInstitution:updateInstitution,
   *    submitOutStandingCondition: submitOutStandingCondition, checkConnectionState : checkConnectionState,
   *    getChallengeQuqestion : getChallengeQuqestion, answerFileThisChallenge: answerFileThisChallenge
   * }}


   */
  var urls = {
    /**
     * Interpolates url strings with values delimited by {{ and }}. Useful for pasting URLs without typos
     * @param {string} url -  the string to interpolate
     * @param {Object=} model - the data to interpolate the string against
     * @returns {string}
     */
    url: function (url, model) {
      var interpolated = url.replace("{{apiBaseUrl}}", self.apiBaseUrl)
      if (!model) return interpolated

      Object.keys(model).forEach(function (key) {
        interpolated = interpolated.replace("{{" + key + "}}", model[key])
      })

      return interpolated
    },

    /**
     * Authenticate to DCC service. Acquires DCC Service auth token. Required for all other requests.
     * @returns {string}
     */
    get login() {
      return this.url("{{apiBaseUrl}}/Login")
    },


    /**
     * Retrieve the customer associated with the current user
     * @returns {string}
     */
    get customer() {
      if (!scope.username)
        throw Error("Set username by calling #fetchToken first");

      return "{{apiBaseUrl}}/customer/{{userName}}"
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{userName}}", scope.username);
    },

    /**
     * Retrieve master list of institutions
     * @returns {string}
     */
    institutions: function () {
      return "{{apiBaseUrl}}/institutions/{{institutionType}}"
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", scope.institutionType);
    },

    /**
     * Add an institution to the current user
     * @param customerId
     * @param institutionId
     * @returns {string}
     */
    addInstitution: function (customerId, institutionId, institutionType, applicationNumber) {

      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/{{institutionId}}/{{applicationNumber}}", {
        customerId: customerId,
        institutionId: institutionId,
        institutionType: institutionType,
        applicationNumber: applicationNumber
      })
    },
    /**
     * Add an Plaid institution 
     * @param customerId
     * @param institutionId
     * @returns {string}
     */
    addPlaidInstitution: function (customerId, institutionId, institutionType, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/{{institutionId}}/link/{{applicationNumber}}", {
        customerId: customerId,
        institutionId: institutionId,
        institutionType: institutionType,
        applicationNumber: applicationNumber
      })
    },
    /**
     * Add an Plaid institution 
     * @param institutionType
     * @returns {string}
     */
    plaidConfiguration: function (institutionType) {
      return this.url("{{apiBaseUrl}}/type/{{institutionType}}/configuration", {
        institutionType: institutionType
      })
    },

    /**
     * Retrieve institutions associated with current user
     * @param customerId
     * @returns {string}
     */
    customerInstitutions: function (customerId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return "{{apiBaseUrl}}/customer/{{customerId}}/{{institutionType}}/account/{{applicationNumber}}"
          .replace("{{customerId}}", customerId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", scope.institutionType)
          .replace("{{applicationNumber}}", applicationNumber);
    },

     /**
     * Retrieve institutions associated with current user
     * @param customerId
     * @returns {string}
     */
    largeDepositTransactions: function (customerId, applicationNumber) {
      return "{{apiBaseUrl}}/customer/{{customerId}}/{{temporaryApplicationNumber}}/transactions/large-deposits"
          .replace("{{customerId}}", customerId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{temporaryApplicationNumber}}", applicationNumber);
    },

    /**
     * Sync W2s and PayStubs
     * @param customerId
     * @param institutionId
     * @returns {string}
     */
    w2sandPaystubSync: function (customerId, institutionId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return "{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/{{institutionId}}/sync/{{applicationNumber}}"
          .replace("{{customerId}}", customerId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", scope.institutionType)
          .replace("{{institutionId}}", institutionId)
          .replace("{{applicationNumber}}", applicationNumber);
    },

    /**
     * Retrieve accounts associated with current user
     * @param customerId
     * @returns {string}
     */
    customerAccounts: function (customerId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return "{{apiBaseUrl}}/customer/{{customerId}}/{{institutionType}}/account/{{applicationNumber}}"
          .replace("{{customerId}}", customerId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", scope.institutionType)
          .replace("{{applicationNumber}}", applicationNumber);
    },

    /**
     * Check connection state
     * @param customerId
     * @returns {string}
     */
    checkConnectionState: function (customerId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/state/{{applicationNumber}}", {
        customerId: customerId,
        institutionType: scope.institutionType,
        applicationNumber: applicationNumber
      })
    },
    /**
     * Get challenge question
     * @param customerId
     * @param institutionId
     * @returns {string}
     */
    getChallengeQuqestion: function (customerId, institutionId, applicationNumber, accountId, connectionId) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionId}}/{{institutionType}}/challenge/{{applicationNumber}}/account/{{accountId}}/connection/{{connectionId}}", {
        customerId: customerId,
        institutionId: institutionId,
        institutionType: scope.institutionType,
        applicationNumber: applicationNumber,
        accountId: accountId,
        connectionId: connectionId
      })
    },

    /**
     * Submit post-assets to complete out-standing condition
     * @param
     * @returns {string}
     */
    submitOutStandingCondition: function (customerId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/all-done/{{applicationNumber}}", {
        customerId: customerId,
        institutionType: scope.institutionType,
        applicationNumber: applicationNumber
      });
    },
    /**
     * Update an institution to the current user
     * @param customerId
     * @param institutionId
     * @returns {string}
     */
    updateInstitution: function (customerId, institutionId, institutionType, applicationNumber) {
      
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/{{institutionId}}/{{applicationNumber}}", {
        customerId: customerId,
        institutionId: institutionId,
        institutionType: institutionType,
        applicationNumber: applicationNumber
      })
    },

    /**
     * Retrieve search list of institutions
     * @returns {string}
     Add a comment to this line
     */
    searchInstitutions: function (filter) {
      filter = filter || "";

      return this.url("{{apiBaseUrl}}/institutions/{{institutionType}}/search?keyword={{keyword}}"
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", scope.institutionType)
          .replace("{{keyword}}", filter));
    },

    /**
     * Submit an answer for a challenge
     * @param customerId
     * @returns {string}
     */
    answerChallenge: function (customerId, institutionType, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/challenge/{{applicationNumber}}", {
        customerId: customerId,
        institutionType: institutionType,
        applicationNumber: applicationNumber
      })
    },

    /**
     * Ad an institution manually
     * @param customerId
     * @returns {string}
     */
    addBankDetail: function (customerId, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";
      
        return this.url("{{apiBaseUrl}}/customer/{{customerId}}/institution/bank-detail/{{applicationNumber}}", {
        customerId: customerId,
        applicationNumber: applicationNumber
      })
    },
    /**
     * Remove an document or account to the current user
     * @param customerId
     * @param institutionType
     * @param accountDocumentId
     * @returns {string}
     */
    removeDocumentOrAccount: function (customerId, accountDocumentId, institutionType, entityType, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return this.url("{{apiBaseUrl}}/customer/{{customerId}}/{{institutionType}}/{{entityType}}/{{accountDocumentId}}/{{applicationNumber}}", {
        customerId: customerId,
        institutionType: institutionType,
        entityType: entityType,
        accountDocumentId: accountDocumentId,
        applicationNumber: applicationNumber
      })
    },
    /**
     * Retrieve institution details with current user
     * @param institutionId
     * @returns {string}
     */
    fetchInstitutionDetail: function (institutionId, institutionType, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";
      
      return "{{apiBaseUrl}}/institutions/{{institutionType}}/{{institutionId}}/{{applicationNumber}}"
          .replace("{{institutionId}}", institutionId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", institutionType)
          .replace("{{applicationNumber}}", applicationNumber);
    },

    fetchAccountTransactions: function (accountId, customerId, applicationNumber) {

      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return "{{apiBaseUrl}}/customer/{{customerId}}/banking/account/{{accountId}}/transactions/{{applicationNumber}}"
        .replace("{{customerId}}", customerId)
        .replace("{{apiBaseUrl}}", self.apiBaseUrl)
        .replace("{{accountId}}", accountId)
        .replace("{{applicationNumber}}", applicationNumber);
    },
    /**
     * Retrieve async account
     * @param institutionId
     * @returns {string}
     */
    fetchAsyncAccount: function (institutionId, accountDocumentId, customerId, institutionType, applicationNumber) {
      if(typeof applicationNumber === "undefined" || applicationNumber == null)
        applicationNumber = "";

      return "{{apiBaseUrl}}/customer/{{customerId}}/institution/{{institutionType}}/{{institutionId}}/account/{{accountDocumentId}}/sync/{{applicationNumber}}"
          .replace("{{customerId}}", customerId)
          .replace("{{institutionId}}", institutionId)
          .replace("{{accountDocumentId}}", accountDocumentId)
          .replace("{{apiBaseUrl}}", self.apiBaseUrl)
          .replace("{{institutionType}}", institutionType)
          .replace("{{applicationNumber}}", applicationNumber);
    }
  };

  var createError = _.curry(function (name, message, originalError) {
    throw _.extend(Error(message), {
      name: name,
      originalError: originalError
    })
  })

  /** @class DCC.Service */
  var dccService = {
    is: "assets-dcc-service",
    behaviors: [alloy.lifeCycleEvents],
    require: ["sessionManager"],
    apiBaseUrl: null,
    attached: function () {
      var docittToken = this.$.sessionManager.get("token");
      var ipAddress = this.$.sessionManager.get("ip-address");
      apiBaseUrl= this.configuration.services.capacityConnect;
      if (!docittToken)
        throw Error("Unable to find docitt token")
      // if (!apiBaseUrl)
      //   throw Error("Unable to find apiBaseUrl")
      // scope.apiBaseUrl = apiBaseUrl;
      scope.docittToken = docittToken;
      scope.ipAddress = ipAddress;
    },

    /**
     * Add a new property to scope object
     * @param {string} property
     * @param {*} value
     */
    updateScope: function (property, value) {
      scope[property] = value;
    },
    fetchToken: fetchToken,

    fetchInstitutions: fetchInstitutions,

    searchInstitutions: searchInstitutions,


    fetchCustomerAccounts: fetchCustomerAccounts,

    addInstitution: addInstitution,
    addPlaidInstitution: addPlaidInstitution,
    plaidConfiguration: plaidConfiguration,
    checkConnectionState: checkConnectionState,
    getChallengeQuqestion: getChallengeQuqestion,
    answerFileThisChallenge: answerFileThisChallenge,

    answerChallenge: answerChallenge,

    fetchInstitutionDetail: fetchInstitutionDetail,
    fetchAccountTransactions: fetchAccountTransactions,

    addBankDetail: addBankDetail,
    removeDocumentOrAccount: removeDocumentOrAccount,
    fetchAsyncAccount: fetchAsyncAccount,
    updateInstitution: updateInstitution,
    submitOutStandingCondition: submitOutStandingCondition,
    fetchCustomerInstitutions: fetchCustomerInstitutions,
    w2sandPaystubSync: w2sandPaystubSync,
    getLargeDepositTransactions: getLargeDepositTransactions
  }

  Polymer(dccService);

  /**
   * @param {jQuery.jqXHR} $request
   * @returns {Promise|Promise<T>}
   */
  function wrap($request) {
    return new Promise(function (resolve, reject) {
      $request.then(resolve).fail(reject);
    })
  }

  function ajax(config) {
    if (typeof config.data === "object")
      config.data = JSON.stringify(config.data)

    return wrap(
        $.ajax(
            _.extend({}, defaultConfig, config)));
  }

  /**
   * Retrieve the DCC auth token so it can be automatically appended to all subsequent requests
   * @returns {Promise<*>}
   */
  function fetchToken() {
    return ajax({
      type: methods.POST,
      url: urls.login,
      cache: false
    }).then(function (response) {
      scope.token = response.result.bearerToken;
    }).catch(createError("FetchTokenError", "Unable to authenticate user"))
  }



  /**
   * Retrieve the master list of institutions
   * @returns {Promise<Array>}
   */
  function fetchInstitutions() {
    return ajax({
      method: methods.GET,
      url: urls.institutions(),
      cache: false
    }).then(_.property("result"))
  }

  /**
   * @method fetchCustomerInstitutions
   * @param {string} customerId - customer's unique id
   * @returns {Promise<Array|Response>}
   */
  function fetchCustomerInstitutions(customerId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";
    
      return ajax({
      method: methods.GET,
      url: urls.customerInstitutions(customerId, applicationNumber),
      cache: false
    });
  }

  /**
   * @method getLargeDepositTransactions
   * @param {string} customerId - customer's unique id
   * @returns {Promise<Array|Response>}
   */
  function getLargeDepositTransactions(customerId, applicationNumber) {
    return ajax({
      method: methods.GET,
      url: urls.largeDepositTransactions(customerId, applicationNumber),
      cache: false
    });
  }

  function w2sandPaystubSync(customerId, institutionId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    return ajax({
      method: methods.PUT,
      url: urls.w2sandPaystubSync(customerId, institutionId, applicationNumber),
      cache: false
    });
  }

  /**
   * Retrieve the search list of institutions
   * @returns {Promise<Array>}
   Add a comment to this line
   */
  function searchInstitutions(filter) {
    return ajax({
      method: methods.GET,
      url: urls.searchInstitutions(filter),
      cache: false
    }).then(_.property("result"))
  }
  /**
   * Add an institution to a customer
   * @param {string} customerId - the customer's id

   * @param {string} institutionId - the institution's id
   * @param {string} username - the customer's username for this institution
   * @param {string} password - the customer's password for this institution
   */
  function addInstitution(customerId, institutionId, username, password, pin, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    assert.equal(typeof customerId, "string")

    assert.equal(typeof institutionId, "string")
    assert.equal(typeof username, "string")
    assert.equal(typeof password, "string")
    assert.ok(customerId.length > 0, "CustomerId cannot be empty")

    assert.ok(institutionId.length > 0, "InstitutionId cannot be empty")
    assert.ok(username.length > 0, "Username cannot be empty")
    assert.ok(password.length > 0, "Password cannot be empty")
    
    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }    

    return ajax({
      method: methods.POST,
      url: urls.addInstitution(customerId, institutionId, institutionType, applicationNumber),
      cache: false,
      data: {
        "CustomerId": customerId,
        "InstitutionId": institutionId,
        "Secret": password,
        "UserName": username,
        "Pin": pin
      }
    }).catch(createError("AddInstitutionError", "Unable to add institution"));
  }
  /**
   * Add an institution to a customer
   * @param {string} customerId - the customer's id

   * @param {string} institutionId - the institution's id
  * @param {string} payload - the customer's payload for this institution
 * @param {string} institutionType - the customer's institutionType for this institution
   */
  function addPlaidInstitution(customerId, institutionId, metadata, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";
    
      if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }    

    return ajax({
      method: methods.POST,
      url: urls.addPlaidInstitution(customerId, institutionId, institutionType, applicationNumber),
      cache: false,
      data: metadata
    }).then(function (response) {
      return response;
    }).catch(createError("AddInstitutionError", "Unable to  add PlaidInstitution"));
  }
  /**
   * get  plaid configuration details
 * @param {string} institutionType - the customer's institutionType for this institution
   */
  function plaidConfiguration(institutionType) {
    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }
    return ajax({
      method: methods.GET,
      url: urls.plaidConfiguration(institutionType)
    }).then(function (response) {
      return response;
    }).catch(createError("AddInstitutionError", "Unable to add plaidConfiguration"));
  }
  /**
   * Check connection state
   * @param {string} customerId - the customer's id
   * @param {string} accountId - account id
   * @param {string} connectionId - connection id
   */
  function checkConnectionState(customerId, accountId, connectionId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    assert.equal(typeof customerId, "string")
    assert.equal(typeof accountId, "string")
    assert.equal(typeof connectionId, "string")

    assert.ok(customerId.length > 0, "CustomerId cannot be empty")
    assert.ok(accountId.length > 0, "AccountId cannot be empty")
    assert.ok(connectionId.length > 0, "Connection cannot be empty")

    return ajax({
      method: methods.POST,
      url: urls.checkConnectionState(customerId, applicationNumber),
      cache: false,
      data: {
        "accountId": accountId,
        "connectionId": connectionId
      }
    }).catch(createError("checkConnectionState", "Unable to check state"));
  }
  /**
   * Get challange question
   * @param {string} customerId - the customer's id
   * @param {string} accountId - account id
   * @param {string} connectionId - connection id
   */
  function getChallengeQuqestion(customerId, institutionId, accountId, connectionId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    assert.equal(typeof customerId, "string")
    assert.equal(typeof institutionId, "string")
    assert.equal(typeof accountId, "string")
    assert.equal(typeof connectionId, "string")

    assert.ok(customerId.length > 0, "CustomerId cannot be empty")
    assert.ok(institutionId.length > 0, "InstitutionId cannot be empty")
    assert.ok(accountId.length > 0, "AccountId cannot be empty")
    assert.ok(connectionId.length > 0, "Connection cannot be empty")

    return ajax({
      method: methods.GET,
      url: urls.getChallengeQuqestion(customerId, institutionId, applicationNumber, accountId, connectionId),
      cache: false,
      /*data: {
        "accountId": accountId,
        "connectionId": connectionId
      }*/
    }).catch(createError("getChallengeQuqestion", "Unable to get challenge question"));
  }

  /**
   * Submit the response to a challenge for filethis
   * @param challenge
   * @param answer
   * @returns {Promise<*>}
   */
  function answerFileThisChallenge(challenge, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    var inputArr = {};

    for (i = 0; i < challenge.responseIds.length; i++) {
      inputArr["\"" + challenge.responseIds[i] + "\""] = challenge.responses[i];
    }
    
    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }

    return ajax({
      method: methods.POST,
      url: urls.answerChallenge(challenge.customerId, institutionType, applicationNumber),
      cache: false,
      data: {
        "applicantId": challenge.customerId,
        "accountId": challenge.accountId,
        "connectionId": challenge.connectionId,
        "interactionId": challenge.interactionId,
        "inputs": inputArr
      }
    })
  }

  /**
   * Update an institution to a customer
   * @param {string} customerId - the customer's id

   * @param {string} institutionId - the institution's id
   * @param {string} username - the customer's username for this institution
   * @param {string} password - the customer's password for this institution
   * @param {string} plaidItemId - the customer's plaidItemId for this institution
   */
  function updateInstitution(customerId, institutionId, username, password, pin, plaidItemId, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    assert.equal(typeof customerId, "string")
    assert.equal(typeof institutionId, "string")
    assert.equal(typeof username, "string")
    assert.equal(typeof password, "string")

    assert.ok(customerId.length > 0, "CustomerId cannot be empty")

    assert.ok(institutionId.length > 0, "InstitutionId cannot be empty")
    assert.ok(username.length > 0, "Username cannot be empty")
    assert.ok(password.length > 0, "Password cannot be empty")
    
    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }

    return ajax({
      method: methods.PUT,
      url: urls.updateInstitution(customerId, institutionId, institutionType, applicationNumber),
      cache: false,
      data: {
        "CustomerId": customerId,
        "InstitutionId": institutionId,
        "Secret": password,
        "UserName": username,
        "Pin": pin,
        "PlaidItemId": plaidItemId
      }
    }).catch(createError("UpdateInstitutionError", "Unable to update institution"));
  }


  /**
   * @typedef {Object} DCC.FetchCustomerAccountsResponse
   * @property {Array<DCC.Account>}
   */

  /**
   * @typedef {Object} DCC.Account
   * @property {string} accountId
   * @property {string} accountNumber
   * @property {string} accountType
   * @property {number} availableBalance
   * @property {number} currentBalance
   * @property {string} customerId
   * @property {string} institutionId
   */

  /**
   * Retrieve the accounts for a specific customer
   * @param customerId
   * @returns {Promise<DCC.FetchCustomerAccountsResponse>}
   */
  function fetchCustomerAccounts(customerId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    return ajax({
      method: methods.GET,
      url: urls.customerAccounts(customerId, applicationNumber),
      cache: false
    }).then(_.property("result"))
  }

  /**
   * @method fetchAsyncAccount
   * @param {string} institutionId - institutionId's unique id , accountDocumentId - account id
   * @returns {Promise<Array|Response>}
   */
  function fetchAsyncAccount(institutionId, accountDocumentId, customerId, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }
    
    return ajax({
      method: methods.PUT,
      url: urls.fetchAsyncAccount(institutionId, accountDocumentId, customerId, institutionType, applicationNumber),
      cache: false
    }).then(function (response) {
          return response;
        })
        .catch(function (error) {
          // Its not sensible to use error for empty collection. Trying to fix this here.
          if (error.status === scope.NOT_FOUND_STATUS)
            return []
          else
            return Promise.reject(error)
        })
  }
  /**
   * submit post-assets to complete the outstanding condition
   * @param customerId
   * @param requestId
   * @param applicationNumber
   * @returns {Promise<*>}
   */
  function submitOutStandingCondition(requestId, applicationNumber, customerId) {
    return ajax({
      method: methods.POST,
      url: urls.submitOutStandingCondition(customerId, applicationNumber),
      cache: false,
      data: {
        "requestId": requestId,
        "createdby": scope.username,
        "entityType": "application",
        "applicationNumber": applicationNumber
      }
    })
  }

  /**
   * Submit the response to a challenge
   * @param challenge
   * @param answer
   * @returns {Promise<*>}
   */
  function answerChallenge(challenge, institutionType, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }

    return ajax({
      method: methods.POST,
      url: urls.answerChallenge(challenge.customerId, institutionType, applicationNumber),
      cache: false,
      data: {
        "publicToken": challenge.publicToken,
        "mfaType": challenge.mfaType,
        "answers": challenge.responses,
        "customerId": challenge.customerId,
        "institutionId": challenge.institutionId
      }
    })
  }



  /**
   * Add an account manually
   * @param customerId
   * @param bankName
   * @param accountType
   * @param accountNumber
   * @param currentBalance
   * @param accountHolderName
   * @returns {Promise<string>}
   */
  function addBankDetail(customerId, bankName, accountType, accountNumber, currentBalance, accountHolderName, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    return ajax({
      method: methods.POST,
      url: urls.addBankDetail(customerId, applicationNumber),
      cache: false,
      data: {
        bankName: bankName,
        accountType: accountType,
        accountNumber: accountNumber,
        currentBalance: currentBalance,
        accountHolderName: accountHolderName
      }
    })
  }

  /**
   * Remove DocumentOrAccount
   * @param customerId
   * @param bankName
   * @param accountType
   * @param accountNumber
   * @param currentBalance
   * @param accountHolderName
   * @returns {Promise<string>}
   */
  function removeDocumentOrAccount(
      accountDocumentId, 
      customerId, 
      institutionType, 
      entityType, 
      applicationNumber) {
    
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";

    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }

    entityType = (typeof entityType !== 'undefined') ?  entityType : "account";
    return ajax({
      method: methods.DELETE,
      cache: false,
      url: urls.removeDocumentOrAccount(customerId, accountDocumentId, institutionType, entityType, applicationNumber),
      data: {}
    })
  }
  /**
   * @method fetchInstitutionDetail
   * @param {string} institutionId - institutionId's unique id
   * @returns {Promise<Array|Response>}
   */
  function fetchInstitutionDetail(institutionId, institutionType) {
    if(_.isUndefined(institutionType))
    {   
      institutionType = scope.institutionType;
    }
    
    return ajax({
      method: methods.GET,
      url: urls.fetchInstitutionDetail(institutionId, institutionType),
      cache: false
    }).then(function (response) {
          return response;
        })
        //        _.property("result"))
        .catch(function (error) {
          // Its not sensible to use error for empty collection. Trying to fix this here.
          if (error.status === scope.NOT_FOUND_STATUS)
            return []
          else
            return Promise.reject(error)
        })
  }

  function fetchAccountTransactions(accountId, customerId, applicationNumber) {
    if(typeof applicationNumber === "undefined" || applicationNumber == null)
      applicationNumber = "";
    
      return ajax({
      method: methods.GET,
      url: urls.fetchAccountTransactions(accountId, customerId, applicationNumber),
      cache: false
    }).then(function (response) {
      return response;
    })
      .catch(function (error) {
        // Its not sensible to use error for empty collection. Trying to fix this here.
        if (error.status === scope.NOT_FOUND_STATUS)
          return []
        else
          return Promise.reject(error)
      })
  }
})(Polymer)