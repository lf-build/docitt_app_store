Polymer({
  is: "assets-forms-challenge",

  properties: {
    mfa:Object,
    institutionId:String,
    publicToken:String,
    mfaType:String,
    responses:{
      type:Array,
      value:[]
      },
    challenges: Array,
    	selectedAnswer:{
						type:Array,
						value:[]
					}
   ,
    challengeAnswer: {
      type: String,
      value: ""
    },
    mfaType:{
      type:String,
      value:""
    },
     mfaTypes: {
            type: Object,
            value: function () {
                return {
                    "SELECTIONS": "SELECTIONS",
                    "DEVICE_LIST": "DEVICE_LIST",
                     "DEVICE": "DEVICE",
                     "QUESTIONS":"QUESTIONS"
                }
            }
        },
          /** @memberOf assetsContainer */
      selectedInstitution: {
        type: Object
      }
  },

  behaviors: [alloy.lifeCycleEvents, alloy.actions],
  attached:function(){
     var self = this;
    self.subscribe('update-selected-answer', self.updateSelectedAnswer.bind(this));
  },
   observers: [
      "initValues(mfa)"
    ],
    initValues:function(mfa){
       this.selectedAnswer = [];
     this.set("publicToken",mfa.public_token);
      this.set("mfaType",mfa.mfa_type);
    },
   updateSelectedAnswer: function (responses) {
     this.selectedAnswer = [];
     if(responses!==null && responses.length>0)
        this.selectedAnswer = responses.join(",");
   
   },
   
  // Check 
  isChallengeMode:function(visibilityMode,mfaType){
 return mfaType.toUpperCase() === visibilityMode;
  },
  /**
   * Reset form data and errors
   * @return {void}
   */
  resetForm: function () {
    this.challengeAnswer = ""
    this.resetError()
  },

  /**
   * This function will reset error to ui-container
   * @return {void}
   */
  resetError: function () {
    console.log('Reset Error');
    this.$$("#challengeAnswerUIContainer").resetServerErrors();
  },

  /**
   * This function will set error of ui-container
   * @return {void}
   */
  setError: function (errorText) {
    console.log('Set Error');
    this.$$("#challengeAnswerUIContainer").setServerErrors(errorText);
  }

})