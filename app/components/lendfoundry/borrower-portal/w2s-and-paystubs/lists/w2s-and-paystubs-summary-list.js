(function () {
    Polymer({
        is: 'w2s-and-paystubs-summary-list',
        properties: {
            titlename:{
                type:String,
                value : "Company"
            },
            entityName:{
                type:String,
                value : "W2PayStub"
            },
            titleText: String, /*Passed from parent for scene title*/
            subtitleText: String, /*Passed from parent for scene title*/
            customerAccounts: { type: Array, value: [] }, /*Summary rows passed from parent*/
            busy: Boolean,
            disableClass: { type: String, value: "" },
            institutions: { type: Array, value: [] }, /*List of institutions to be displayed in bottom dropdown*/
            customerInstitutions: Array,  /*List of customer institutions to be displayed*/
            inProgress: Boolean, /*This will be used to disable buttons*/
            institutionsWithW2sAndPayStubs: { type: Array, value: [] },
            customerId: { type: String, value: "" },
            institutionType: String,
            applicationNumber: String,
        },
        behaviors: [eventHub, ensureDependenciesBehavior, alloy.lifeCycleEvents, alloy.actions],

        observers: ['_filterW2sAndPayStubsData(customerInstitutions)'],

        computeIsEmpty: function (array) {
            if (!array)
                return true;
            return array.length === 0;
        },


        _displayLogo: function (value) {
            if (value === 0) return true;
            else return false;
        },

        checkoutLength: function(documentList){
            return (documentList[0].documents.length > 1 && documentList[0].documents != null);
        },
        handleDownloadDocumentClick: function (e) {
            var self = this;
            var entityType = "filethis";
            var entityId = e.target.dataset.entityId;
            var documentId = e.target.dataset.id || "";
            if (!documentId) {
                console.warn("document id is blank");
                return;
            }
            this.ensure("documentManagerService")
                .then(function (documentManagerService) {
                    return documentManagerService.getDocumentByEntityTypeWithId(entityType, encodeURIComponent(entityId), documentId);
                })
                .then(function (document) {
                    self.downloadDocument(document, entityType, encodeURIComponent(entityId), documentId);
                }).catch(function (error) {
                    var errorSummary = "";
                    if (error && error.responseJSON && error.responseJSON.message) {
                        errorSummary = error.responseJSON.message;
                    }
                    console.warn(errorSummary);
                });

        },


        getDataId: function (parent_index, child_index) {
            return parent_index + '-' + child_index;
        },


        downloadDocument: function (document, entity, entityId, documentId) {
            if(this.configuration.services.documentManager == undefined){
                throw new Error("documentManager service not found");
            }
            var baseUrl = this.configuration.services.documentManager;
            var url = [baseUrl, entity, entityId, documentId, "download"].join("/");
            var fileName = "";
            if (document != null)
                fileName = document.fileName || "";
            var x = new XMLHttpRequest();
            x.open("GET", url, true);
            x.responseType = "blob";
            x.setRequestHeader("Authorization", "Bearer " + this.$.session.get("token"));
            x.onload = function (e) {
                download(e.target.response, fileName);
            };
            x.send();
        },


        _filterW2sAndPayStubsData: function (customerInstitutions) {
            $("#divPayStubsUpload").hide();
            $("#divPayStubsGrid").hide();
            $(this.$$("#liNew")).find('input:text').val('');
            $(this.querySelectorAll("#w2sEditList")).hide();
            $(this.querySelectorAll("#payStubEditList")).hide();
            $(this.querySelectorAll("#w2sList")).show();
            $(this.querySelectorAll("#payStubsList")).show();

            var self = this;
            self.set("busy", false);
            self.set("disableClass", "");
            var items = self.customerAccounts;
            var institutions = self.institutions;
            var institutionsWithW2sAndPayStubs = [];
            self.institutionsWithW2sAndPayStubs = customerInstitutions;
        },

        EditInfo: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-w2s-" + id);
            $(liW2s).hide();
            var liW2S1 = this.$$(".li-li-w2s-" + id);
            $(liW2S1).show();
        },
        CancelInfo: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-w2s-" + id);
            $(liW2s).show();
            var liW2S1 = this.$$(".li-li-w2s-" + id);
            $(liW2S1).hide();
            this.fire("w2s-and-paystubs-documents-remove");
        },
        CancelInfoNew: function (e) {
            $(this.$$("#liNew")).find('input:text').val('');
            this.fire("w2s-and-paystubs-documents-remove");
        },
        w2sClass: function (parent_index, child_index) {
            return 'secndrow clearfix li-w2s-' + parent_index + '-' + child_index;
        },
        w2sClassEdit: function (parent_index, child_index) {
            return 'secndrow clearfix div-margin li-li-w2s-' + parent_index + '-' + child_index;
        },
        payStubClass: function (parent_index, child_index) {
            // return 'secndrow clearfix li-paystub-' + parent_index + '-' + child_index;
            return 'row clearfix li-paystub-' + parent_index + '-' + child_index;
        },
        payStubClassEdit: function (parent_index, child_index) {
            // return 'secndrow clearfix div-margin li-li-paystub-' + parent_index + '-' + child_index;
            return 'row clearfix li-li-paystub-' + parent_index + '-' + child_index;
        },
        EditInfoPayStub: function (e) {
            var id = e.target.dataset.id;
            var liPayStub = this.$$(".li-paystub-" + id);
            $(liPayStub).hide();
            var liPayStub1 = this.$$(".li-li-paystub-" + id);
            $(liPayStub1).show();
            $(liPayStub).parent().parent().find("#liEditInstitutionName").show();
            $(liPayStub).parent().parent().find("#liInstitutionName").hide();

        },
        CancelInfoPayStub: function (e) {
            var id = e.target.dataset.id;
            var liPayStub = this.$$(".li-paystub-" + id);
            $(liPayStub).show();
            var liPayStub1 = this.$$(".li-li-paystub-" + id);
            $(liPayStub1).hide();
            this.fire("w2s-and-paystubs-documents-remove");
            $(liPayStub).parent().parent().find("#liEditInstitutionName").hide();
            $(liPayStub).parent().parent().find("#liInstitutionName").show();
        },
        getValue: function (value) {
            if (value)
                return value;
            else return "";
        },
        getDateFormattedValue: function (yearDate) {
            var year = yearDate.split(' ')[1];
            return "01/01/" + year;
        },
        displaySyncDiv: function (e) {
            if (e.target.getAttribute("disabled") === null) {
                $("#divPayStubsUpload").hide();
                $("#divPayStubsGrid").show();
            }
        },
        displayUploadDiv: function (e) {
            if (e.target.getAttribute("disabled") === null) {
                $("#divPayStubsGrid").hide();
                $("#divPayStubsUpload").show();

                if (e.target.dataset && e.target.dataset.id) {
                    var id = e.target.dataset.id;
                    var liPayStub = this.$$(".li-paystub-" + id + '-0');
                    $(liPayStub).hide();
                    var liPayStub1 = this.$$(".li-li-paystub-" + id + '-0');
                    $(liPayStub1).show();
                }
            }
        },
        handleW2sSaveClick: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-li-w2s-" + id);
            var self = this;
            self.isValid(liW2s)
                .then(function (isValid) {
                    if (isValid) {
                        self.set("busy", true);
                        self.set("disableClass", "no-click");
                        var institutionId = e.target.dataset.institutionId;
                        var customerId = e.target.dataset.customerId;

                        this.W2sAndPayStubsSave(liW2s, institutionId, customerId);
                    }
                }.bind(self));
        },
        handlePaystubsSaveClick: function (e) {
            var id = e.target.dataset.id;
            var liW2s = this.$$(".li-li-paystub-" + id);
            var self = this;
            self.isValid(liW2s)
                .then(function (isValid) {
                    if (isValid) {
                        self.set("busy", true);
                        self.set("disableClass", "no-click");
                        var institutionId = e.target.dataset.institutionId;
                        var customerId = e.target.dataset.customerId;
                        this.W2sAndPayStubsSave(liW2s, institutionId, customerId);
                    }
                }.bind(self));
        },
        handleNewSaveClick: function (e) {
            var liW2s = this.$$("#liNew");
            var self = this;
            self.isValid(liW2s)
                .then(function (isValid) {
                    if (isValid) {
                        self.set("busy", true);
                        self.set("disableClass", "no-click");
                        var customerId = self.customerId;
                        this.W2sAndPayStubsSave(liW2s, "", customerId);
                    }
                }.bind(self));
        },
        isValid: function (liW2s) {
            var self = this;
            //Validate All ui elements
            var _validations = [];

            liW2s.querySelectorAll('ui-container').forEach(function (_uiInputContainer) {
                _validations.push(_uiInputContainer.isValid());
            });

            //We do not need to chain validation here as they are not dependent on each other.
            return Promise.all(_validations)
                .then(function (validationResult) {
                    if (validationResult.indexOf(false) == -1) {
                        //Validation OK on the screen go ahead
                        return true;
                    } else {
                        //Validation Failed
                        return false;
                    }
                });
        },
        W2sAndPayStubsSave: function (liW2s, institutionId, customerId) {
            if(institutionId === "")
            {
                var w2sInstitutionName = $(liW2s).find("#w2sInstitutionName").val();                
            }
            else
            {
                var w2sInstitutionName = $(liW2s).parent().parent().find("#liEditInstitutionName").find("#editInstitutionName").val();                
            }   
            var w2sInstitutionOwner = $(liW2s).find("#w2sInstitutionOwner").val();
            var w2sDate = $(liW2s).find("#w2sDate").val();
            var w2sCompany = $(liW2s).find("#w2sCompany").val();
            var w2sTotalAmount = $(liW2s).find("#w2sTotalAmount").val();
            if (w2sTotalAmount && w2sTotalAmount.indexOf('$') != -1) {
                w2sTotalAmount = w2sTotalAmount.replace('$', '');
                w2sTotalAmount = w2sTotalAmount.trim();
            }  

            var metadata = JSON.stringify({ 'Id': institutionId, 'Owner': w2sInstitutionOwner, 'Institution': w2sInstitutionName, 'Monthyear': w2sDate, 'Type': 'Saving', 'Total': w2sTotalAmount, 'Company': w2sCompany });
            var payload = {};
            payload.metadata = metadata;
            if(this.configuration.services.capacityConnect == undefined){
                throw new Error("capacityConnect service not found");
            }
            payload.targetUrl = this.configuration.services.capacityConnect + "/customer/" + customerId + "/W2PayStub/manual/" + this.applicationNumber;
            payload.submitUrl = "/customer/" + customerId + "/W2PayStub/manual/" + this.applicationNumber;

            this.fire("w2s-and-paystubs-save", payload);
        },
        handleNextClick: function (e) {
            $("#divPayStubsUpload").hide();
            $("#divPayStubsGrid").hide();
            this.set("busy", false);
            this.set("disableClass", "");
            $(this.$$("#liNew")).find('input:text').val('');
            $(this.querySelectorAll("#w2sEditList")).hide();
            $(this.querySelectorAll("#payStubEditList")).hide();
            $(this.querySelectorAll("#w2sList")).show();
            $(this.querySelectorAll("#payStubsList")).show();
            this.fire("w2s-and-paystubs-submit");
        },
        getFormattedAmount: function (listOfDocuments) {
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].total)
                return '$' + listOfDocuments[0].total;
            else return "";
        },

        getAsyncClass: function (value, state) {
            if (state == "not-connected") return "no-click";
            if (value) return "no-click"; else return "active";
        },

        getUploadClass: function (value, state) {
            if (state == "not-connected") return "no-click";
            if (value) return "active"; else return "no-click";
        },

        getStatusClass: function (value) {
            if (value == "connected") return "df df-tick"; else return "df df-alert";
        },
        getFileId: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].fileId;
            else
                return "";
        },
        getDocumentId: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].documentId;
             else
                return "";
        },
        getFileName: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].fileName;
            else
                return "";
        },
        getDocuments: function(listOfDocuments){
            if(listOfDocuments && listOfDocuments.length > 0)
                return listOfDocuments[0].documents;
            else
                return "";
        },
        getName: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].name)
                return listOfDocuments[0].name;
            else return "";
        },
        getDocumentSubType: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].documentSubType)
               return listOfDocuments[0].documentSubType;
            else return "";
        },
        getCompany: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].company)
            return listOfDocuments[0].company;
         else return "";
        },
        getYearDate: function(listOfDocuments){
            if (listOfDocuments && listOfDocuments.length > 0 && listOfDocuments[0].yearDate)
             return listOfDocuments[0].yearDate;
            else return "";
        }
    });
})()