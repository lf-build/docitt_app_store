Polymer({
  is: "assets-filethis-forms-challenge",

  properties: {
    applicantId: String,
    accountId: String,
    connectionId: String,
    interactionId: String,
    titleText: String,
    subtitleText: String,
    mfa: Object,
    mfaType: String, // code value from response
    mfaTypes: {
      type: Object,
      value: function () {
        return {
          "CREDENTIALS": "CREDENTIALS",
          "GENERIC": "GENERIC",
          "SELECTIONS": "SELECTIONS",
          "DEVICE_LIST": "DEVICE_LIST",
          "DEVICE": "DEVICE",
          "QUESTIONS": "QUESTIONS"
        }
      }
    },
    mfaType: {
      type: String,
      value: ""
    },
    institutionId: String,
    responseIds: {
      type: Array,
      value: []
    },
    responses: {
      type: Array,
      value: []
    },
    selectedAnswerId: {
      type: Array,
      value: []
    },
    selectedAnswer: {
      type: Array,
      value: []
    }
    ,
    challengeAnswer: {
      type: String,
      value: ""
    },
    /** @memberOf assetsContainer */
    selectedInstitution: {
      type: Object
    }
  },

  behaviors: [alloy.lifeCycleEvents, alloy.actions, eventHub],
  attached: function () {
    var self = this;
    self.subscribe('update-selected-answer', self.updateSelectedAnswer.bind(this));
  },
  observers: [
    "initValues(mfa)"
  ],
  initValues: function (mfa) {
    this.selectedAnswerId = [];
    this.selectedAnswer = [];
    this.responseIds = [];
    this.responses = [];
    this.set("mfaType", mfa.mfa_type);
  },
  handleInput: function (event) {
    var self = this;
    // store current positions in variables
    
    if(event.target.type === "radio")
      self.responseIds[event.model.que_no] = event.target.name;
    else
      self.responseIds[event.model.que_no] = event.target.id;

    self.responses[event.model.que_no] = event.target.value;    
    self.publish('update-selected-answer', self.responses);
  },
  updateSelectedAnswer: function (responses) {
    var self = this;
    this.selectedAnswerId = [];
    this.selectedAnswer = [];
    if (responses !== null && responses.length > 0)
    {
      this.selectedAnswerId = self.responseIds.join(",");
      if(this.selectedAnswerId.charAt(0) == ",") {
        this.selectedAnswerId = this.selectedAnswerId.substring(1);
      }
      this.selectedAnswer = responses.join(",");
      if(this.selectedAnswer.charAt(0) == ",") {
        this.selectedAnswer = this.selectedAnswer.substring(1);
      }
    }
  },
  // Check 
  checkKind: function (kind, txtKind) {
    return kind === txtKind;
  },
  /**
   * Reset form data and errors
   * @return {void}
   */
  resetForm: function () {
    this.challengeAnswer = ""
    this.resetError()
  },
  /**
   * This function will reset error to ui-container
   * @return {void}
   */
  resetError: function () {
    console.log('Reset Error');
    // this.$$("#challengeAnswerUIContainer").resetServerErrors();
  },
  /**
   * This function will set error of ui-container
   * @return {void}
   */
  setError: function (errorText) {
    console.log('Set Error');
    // this.$$("#challengeAnswerUIContainer").setServerErrors(errorText);
  }
})