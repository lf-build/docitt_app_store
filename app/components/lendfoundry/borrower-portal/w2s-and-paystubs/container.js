(function () {
    /**
     * @class assetsContainer
     * @mixes alloy.lifeCycleEvents
     */
    Polymer({
        is: "w2s-and-paystubs-container",
        behaviors:
        [
            alloy.lifeCycleEvents,
            alloy.actions,
            alloy.visibilityController
        ],
        properties: {
            visibleButton: {
                type: Boolean,
                value: true
            },
            refreshSummaryList: {
                type: Boolean,
                value: true
            },
            baseUrl: {
                type: String,
                value: null
            },
            customer: {
                type: Object,
                value: function () {
                    return {}
                }
            },
            customerId: {
                type: String,
                value: ""
            },
            accountId: {
                type: String,
                value: ""
            },
            connectionId: {
                type: String,
                value: ""
            },
            interactionId: {
                type: String,
                value: ""
            },
            invitationId: {
                type: String,
                value: ""
            },
            providerId: {
                type: String,
                value: ""
            },
            institutions: {
                type: Array,
                value: function () {
                    return []
                }
            },
            /** @memberOf assetsContainer */
            filterInstitutions: {
                type: Array,
                value: function () {
                return []
                }
            },
            customerInstitutions: {
                type: Array,
                value: function () {
                    return []
                }
            },
            customerAccounts: {
                type: Array,
                value: function () {
                    return []
                }
            },
            /**
             * @property
             */
            selectedInstitutionId: {
                type: String,
                value: ""
            },
            challenges: {
                type: Array,
                value: function () {
                    return []
                }
            },
            /** @memberOf assetsContainer */
            mfa: Object,
            institutionId: String,
            loggedInUsername: String,
            events: {
                type: Object,
                value: function () {
                    return {
                        SELECT: "assets-select",
                        SUBMIT: "assets-submit",
                        SET_VISIBILITY_MODE: "assets-set-visibility-mode",
                        ANSWER: "assets-answer-challenge"
                    }
                },
            },
            visibilityModes: {
                type: Object,
                value: function () {
                    return {
                        INIT: "INIT",
                        ERROR: "ERROR",
                        LOADING: "LOADING",
                        FORM: "FORM",
                        GRID: "GRID",
                        CHALLENGE: "CHALLENGE",
                        LIST: "LIST",
                        UPLOADMANUALLY: "UPLOADMANUALLY"
                    }
                }
            },
            activeVisibilityMode: {
                type: String,
                value: "INIT",
                observer: '_activeVisibilityModeChanged'
            },
            error: {
                type: Object
            },
            /*Need to pass to DCC endpoints*/
            institutionType: {
                type: String,
                value: 'W2PayStub'
            },
            /**
             * out-standing condition for which this component is loaded
             */
            outStandingCondition: Object,
            /**
             * application number associated with post-application
             */
            applicationNumber: String,
            /**
             * To disable buttons
             */
            inProgress: {
                type: Boolean,
                value: false
            },
            actionApplicantId: String,
            actionDocumentId: String
        },
        prettyPrint: function (json) {
            return JSON.stringify(json, null, 2)
        },
        /**
         * Update width of parent as per visibility mode
         */
        _activeVisibilityModeChanged: function (newValue, oldValue) {
            var widthToChange;
            switch (newValue) {
                case this.visibilityModes.LIST:
                case this.visibilityModes.GRID:
                    widthToChange = 90;
                    break;
                default:
                    widthToChange = 90;
            }
            this.fire("required-condition-adjust-width", widthToChange);
        },
        observers: [
            'initValues(selectedValues)'
        ],
        require: [
            "dccService",
            "addUpdateDCCInstitution",
            "answerChallenge",
            "progressBar"
        ],
        listeners: {
            "filethis-fix-connection": "handleFileThisFixConnection",
            "assets-select": "handleSelect",
            "assets-submit": "handleSubmit",
            "assets-answer-challenge": "handleAnswerChallenge",
            "assets-set-visibility-mode": "handleSetVisibilityMode",
            "skip-condition": "handleSkip",
            "w2s-and-paystubs-submit": "handleW2sAndPayStubsSubmit",
            "upload-manually": "handleUploadManually",
            "w2s-and-paystubs-remove": "handleW2sAndPayStubsRemove",
            "w2s-and-paystubs-save": "handleW2sAndPayStubsSave",
            "w2s-and-paystubs-refresh": "handleW2sAndPayStubsRefresh",
            "w2s-and-paystubs-documents-remove": "handleW2sAndPayStubsDocumentsRemove",
            "confirm": "handleConfirmDelete",
            "handleW2sAndPayStubsSync": "handleW2sAndPayStubsSync"
        },

        handleFileThisFixConnection: function (event) {
            var self = this;
            var state = _.get(event, "detail.actionState");

            if (state == "connected")
                return false;

            self.selectedInstitutionId = _.get(event, "detail.actionInstitutionId");
            self.customerId = _.get(event, "detail.actionApplicantId");
            self.accountId = _.get(event, "detail.actionAccountId");
            self.connectionId = _.get(event, "detail.actionConnectionId");

            self.ensure("dccService")
                .then(function (service) {
                    /**
                     * Check State
                     */
                    service
                        .checkConnectionState(
                            self.customerId, 
                            self.accountId, 
                            self.connectionId,
                            self.applicationNumber)
                        .then(function (response) {
                            if(!_.isUndefined(response))
                            {
                                if (response.state == "question") {
                                    self.$.dccService
                                        .getChallengeQuqestion(
                                            self.customerId,
                                            self.selectedInstitutionId,
                                            self.accountId,
                                            self.connectionId,
                                            self.applicationNumber)
                                        .then(function (response) {
                                            if (!_.isUndefined(response.mfa)) {
                                                self.mfa = response.mfa;
                                                self.interactionId = response.interactionId;
                                                return self.showChallenge();
                                            }
                                        });
                                }
                                else{
                                    self.showLoading();
                                    setTimeout(() => {
                                        self.init();    
                                    },10000);
                                }
                            }
                        });
                });
        },

        openConfirmModal: function () {
            this.$.confirm.open();
        },

        handleConfirmDelete: function () {
            var self = this;
            if (_.isUndefined(self.actionDocumentId || self.actionApplicantId))
            return;
            self.$.dccService.removeDocumentOrAccount(
                self.actionDocumentId, 
                self.actionApplicantId,
                self.institutionType,
                "account", 
                self.applicationNumber)
                .then(function () {
                    self.showLoading();
                    setTimeout(() => {
		                self.init();    
		            },10000);
                });
        },

        handleW2sAndPayStubsSync: function (event) {
            var self = this;
            var institutionId = _.get(event, "detail.actionInstitutionId");
            var applicantId = _.get(event, "detail.actionApplicantId");

            self.$.dccService.w2sandPaystubSync(applicantId, institutionId, self.applicationNumber)
                .then(function () {
                    self.showLoading();
                    setTimeout(() => {
		                self.init();    
		            },10000);
                });
        },

        handleSelect: function (event) {
            var id = _.get(event, "detail.actionInstitutionId")
            if (!id)
                return console.warn("Select called with invalid institutionsId of " + id)

            this.selectedInstitutionId = id;
            var self = this;
            this.$.dccService.fetchInstitutionDetail(id).then(function (response) {
                self.selectedInstitution = response;
                self.selectedInstitutionId = id;
            
                $(self.$$("#divPayStubsUpload")).hide();
                $(self.$$("#divPayStubsGrid")).hide();
                
                self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.FORM })
            });

            // this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.FORM })
        },
        handleSubmit: function (event) {
            var self = this;
            var form = $("assets-forms-credentials")[0];

            return form.isValid()
                .then(function (isValid) {
                    if (!isValid) return
                    var formData = form.getFormData();
                    var password = "";
                    var username = "";
                    var pin = "";
                    var loggedinUsername = _.get(event, "detail.actionLoggedinUsername");
                    if (formData != null) {
                        password = formData.password || "";
                        username = formData.username || "";
                        pin = formData.pin || "";
                    }
                    form.reset()
                    self.showLoading()

                    /** @type {Interactors.AddUpdateDCCInstitution} */
                    var addUpdateDCCInstitution = self.$.addUpdateDCCInstitution

                    /** @type {Interactors.AddUpdateDCCInstitutionContext} */
                    // self.selectedInstitutionId = "100100";

                    var context
                    context = {
                        customerId: self.customerId,
                        institutionId: self.selectedInstitutionId,
                        password: password,
                        username: username,
                        pin: pin,
                        plaidItemId: self.selectedPlaidItemId,
                        applicationNumber: self.applicationNumber
                    }

                    addUpdateDCCInstitution.execute(context).then(function (result) {
                        if (result != null) {
                            if (!result.isSuccess) {
                                self.accountId = result.fileThisAccountId;
                                self.connectionId = result.connectionId;
                                var timeCounter = 0;
                                var interval = setInterval(function () {
                                    self.$.dccService.checkConnectionState(
                                            self.customerId, 
                                            self.accountId, 
                                            self.connectionId,
                                            self.applicationNumber)
                                    .then(function (response) {
                                        if(!_.isUndefined(response))
                                        {
                                            var state = response.state;
                                            timeCounter = timeCounter + 1;
                                            if (state == "question") {
                                                clearInterval(interval);
                                                self.$.dccService.getChallengeQuqestion(
                                                    self.customerId, 
                                                    self.selectedInstitutionId, 
                                                    self.accountId, 
                                                    self.connectionId,
                                                    self.applicationNumber)
                                                .then(function (response) {
                                                    if (!_.isUndefined(response.mfa)) {
                                                        self.mfa = response.mfa;
                                                        self.interactionId = response.interactionId;
                                                        // self.institutionId = response.institutionId;
                                                        return self.showChallenge();
                                                    }
                                                });
                                            }
                                            if (state === "completed" || state === "uploading" || state === "waiting") {
                                                clearInterval(interval);
                                                //
                                                self.showLoading();
                                                // self.$$("#challengeForm").resetForm();

                                                //TODO: Required Changes
                                                setTimeout(() => {
                                                    self.fetchCustomerInstitutions(self.customerId, self.applicationNumber)
                                                    .then(function (customerInstitutions) {
                                                        self.customerInstitutions = customerInstitutions;
                                                        self.fetchCustomerInstitutionsSuccess.bind(self)
                                                    })
                                                    .then(function () {
                                                        self.showList();
                                                    });
                                                }, 3000);
                                            }
                                            if (timeCounter >= 12) {
                                                clearInterval(interval);
                                                timeCounter = 0;
                                                return self.showList();
                                            }
                                        }
                                    });
                                }, 5000);
                            } else {
                                return self.showList();
                            }
                        }
                    })
                        .catch(function (error) {
                            self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.FORM })
                            self.handleError(error)
                        })
                })
        },
        handleAnswerChallenge: function (event) {
            // //Reset errors
            // self.$$("#challengeForm").resetError();
            // var form = $("assets-filethis-forms-challenge")[0];
            // form.reset()
            var self = this;
            self.showLoading()

            var responseIds = _.get(event, "detail.actionChallengeSelectedAnswerId");
            var responses = _.get(event, "detail.actionChallengeSelectedAnswer");
            responseIds = responseIds.split(",");
            responses = responses.split(",");

            var challenge = {
                "customerId": self.customerId,
                "accountId": self.accountId,
                "connectionId": self.connectionId,
                "interactionId": self.interactionId,
                "responseIds": responseIds,
                "responses": responses
            }

            this.$.answerChallenge.execute({
                challenge: challenge
            }).then(function (result) {
                var timeCounter = 0;

                setTimeout(function () {
                    var interval = setInterval(function () {
                        self.$.dccService.checkConnectionState(
                            self.customerId, 
                            self.accountId, 
                            self.connectionId,
                            self.applicationNumber)
                        .then(function (response) {
                            if(!_.isUndefined(response))
                            {
                                var state = response.state;
                                timeCounter = timeCounter + 1;
                                if (state == "question") {
                                    clearInterval(interval);
                                    self.$.dccService.getChallengeQuqestion(
                                        self.customerId, 
                                        self.selectedInstitutionId, 
                                        self.accountId, 
                                        self.connectionId,
                                        self.applicationNumber)
                                    .then(function (response) {
                                        if (!_.isUndefined(response.mfa)) {
                                            self.mfa = response.mfa;
                                            self.interactionId = response.interactionId;
                                            // self.institutionId = response.institutionId;
                                            return self.showChallenge();
                                        }
                                    });
                                }
                                if (state === "completed" || state === "uploading" || state === "waiting") {
                                    clearInterval(interval);
                                    //
                                    self.showLoading();
                                    //self.$$("#challengeForm").resetForm();

                                    //TODO: Required Changes
                                    setTimeout(() => {
                                        self.fetchCustomerInstitutions(self.customerId, self.applicationNumber)
                                        .then(function (customerInstitutions) {
                                            self.customerInstitutions = customerInstitutions;
                                            self.fetchCustomerInstitutionsSuccess.bind(self)
                                        })
                                        .then(function () {
                                            self.showList();
                                        });
                                    }, 3000);
                                }
                                if (timeCounter >= 12) {
                                    clearInterval(interval);
                                    timeCounter = 0;
                                    return self.showList();
                                }
                            }
                        });
                    }, 5000);
                }, 20000);
            })
                .catch(function (error) {
                    self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.FORM })
                    self.handleError(error)
                })
        },
        handleSetVisibilityMode: function (event) {
            var self = this;
            var progressBar = this.$.progressBar;
            /** @type {string} */
            var nextVisibilityMode;
            nextVisibilityMode =
                _.get(event, "detail.actionVisibilityMode")
                || _.get(event, "detail.visibilityMode");
            var delay = 0;
            var LOADING = self.visibilityModes.LOADING;
            var GRID = self.visibilityModes.GRID;
            var LIST = self.visibilityModes.LIST;

            // If progress bar is already showing, set it to complete before changing visibility mode
            if (self.activeVisibilityMode === LOADING && nextVisibilityMode !== LOADING) {
                // Show complete progress bar before hiding
                progressBar.finish()
                    .then(function () {
                        self.changeVisibilityMode(nextVisibilityMode)
                    })
            }

            // Ensure progress bar is started after it is visible
            if (nextVisibilityMode === LOADING) {
                progressBar.start()
            }

            // Fetch institutions before showing grid
            if (nextVisibilityMode === GRID) {
                return self.fetchInstitutions()
                    .then(function (institutions) {
                        self.institutions = institutions
                        // self.changeVisibilityMode(nextVisibilityMode)
                        self.changeVisibilityMode("LIST")
                    })
                    .catch(self.handleError);
            }

            //Summary
            if (nextVisibilityMode === LIST) {
                //TODO: Changes Required
                self.changeVisibilityMode(nextVisibilityMode)
                self.fire("required-condition-adjust-width", 90);
            }

            setTimeout(function () {
                self.changeVisibilityMode(nextVisibilityMode)
            }, delay);
        },
        handleSkip: function () {
            this.moveToNextCondition('skipped');
        },
        /**
         * Will be called on click of I'M Done 
         */
        handleW2sAndPayStubsSubmit: function () {
            //
            var self = this;
            self.inProgress = true;

            self.$.dccService.submitOutStandingCondition(
                self.outStandingCondition.id, 
                self.applicationNumber, 
                self.customerId)
            .then(function () {
                self.moveToNextCondition('completed');
                self.inProgress = false;
            });
        },
        handleUploadManually: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.UPLOADMANUALLY })
        },
        /**
        * Will be called on click of Remove
        */
        handleW2sAndPayStubsRemove: function (event) {
            var self = this;

            var id = _.get(event, "detail.actionInstitutionId")
            if (!id)
                return console.warn("Select called with invalid institutionsId of " + id);

            self.selectedInstitutionId = id;
            var actionFileId = _.get(event, "detail.actionFileId");
            var actionInstitutionId = _.get(event, "detail.actionInstitutionId");
            self.actionDocumentId = _.get(event, "detail.actionDocumentId");
            self.actionApplicantId = _.get(event, "detail.actionApplicantId");

            this.openConfirmModal();
        },
        handleW2sAndPayStubsSave: function (event) {
            var self = this;
            if (event.detail) {
                if ($(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').filesAdded) {
                    var payload = {};
                    payload.metadata = event.detail.metadata;
                    payload.targetUrl = event.detail.targetUrl;
                    this.set("payload", payload);
                    $(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').processQueue(payload);
                    self.showLoading();
                    setTimeout(() => {
		                self.init();    
		            },10000);
                }
                else {
                    var payloadForm = new FormData();
                    payloadForm.append('metadata', event.detail.metadata);
                    payloadForm.append('files', '');
                    this.ensure("customHttpRequestService")
                        .then(function (service) {
                            return service.post(event.detail.submitUrl, payloadForm, self.configuration.services.capacityConnect);
                        })
                        .then(function (response) {
                            self.showLoading();
                            self.init();
                        })
                        .catch(function (error) {
                            console.warn(error);
                        });
                }
            }
        },
        handleW2sAndPayStubsRefresh: function (event) {
            var self = this;
            self.init();
        },
        handleW2sAndPayStubsDocumentsRemove: function (event) {
            if (this.$$("#manualDocument") != null)
                $(this.$$("#manualDocument")).find("#addDocument")[0].querySelector('#dropZone').removeAllDocuments();
        },

        /**
        * Prepares dcc service for use
        * @returns {Promise<*>}
        */
        initDcc: function () {
            var self = this;
            return this.ensure("initDcc")
                .then(function (initDcc) {
                    return initDcc.execute()
                })
        },
        fetchCustomerInstitutionsSuccess: function (result) {
            var self = this

            self.customerAccounts = result.customerAccounts;
            self.institutions = result.institutions;
            self.customerInstitutions = result.customerInstitutions;
            self.customerAccountsByInstitution = result.customerAccountsByInstitution;
            self.customerAccountsListData = _.values(self.customerAccountsByInstitution);
        },
        storeCustomer: function (customer) {
            this.customerId = customer.customerId;
            this.invitationId = customer.invitationId;
            this.providerId = customer.providerId;
        },
        storeInstitutions: function (institutions) {
            this.institutions = institutions
        },

        attached: function () {
            if(this.configuration.services.capacityConnect == undefined){
                throw new Error("capacityConnect service not found");
            }
            this.baseUrl = this.configuration.services.capacityConnect;
            this.init();
        },
        init: function () {
            var self = this;

            Promise.all([
                this.initDcc()
            ]).then(function (responses) {
                // var [{ customer }] = responses;
                self.customer = responses[0].customer;
                self.loggedInUsername = responses[0].username;
                if (!self.customerId)
                    self.customerId = self.loggedInUsername;

                //self.customerInstitutions = '[{"institutionId":"100100","name":null,"logo":null,"logoUrl":null,"listOfDocuments":[{"documentId":"18770027","fileId":"992525aee11f4033a44ca5d54d3a759a","fileName":"rahul.g+23@sigmainfo.net-Test Site - All in one-Statement-01-10-2017-18770027.pdf","isLinked":false,"isManualUpload":false,"type":null,"yearDate":null,"name":null,"total":null,"company":null,"documents":[],"isSubmitted":false}],"connectionId":"404326","fileThisAccountId":"143008","applicantId":"rahul.g+23@sigmainfo.net","isLinked":true,"isSuccess":false,"successMessage":null}]';
            })
                .then(function () {
                    return self.fetchInstitutions()
                }).then(function (institutions) {
                    self.institutions = institutions
                }).then(function () {
                    return self.searchInstitutions("")
                }).then(function (searchResponse) {
                    self.set("filterInstitutions", []);
                    self.set("filterInstitutions", searchResponse);
                })
                .then(function () {
                    self.fetchCustomerInstitutions(self.customerId, self.applicationNumber)
                    .then(function (customerInstitutions) {
                        self.customerInstitutions = customerInstitutions;
                    });
                })
                //.then(function () {
                //    return self.$.dccService
                //        .fetchCustomerAccounts(self.loggedInUsername);
                //})
                .then(function () {
                    self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.LIST })
                    self.fire("required-condition-adjust-width", 90);
                })
        },
        handleFilterInstitutions: function (filter) {
            var self = this;
            return self.searchInstitutions(filter).then(function (searchResponse) {
                self.set("filterInstitutions", []);
                self.set("filterInstitutions", searchResponse);
                return searchResponse;
            });
        },
        /**
        * Retrieve search list of available institutions
        * @returns {Promise<Array<object>>}
        */
        searchInstitutions: function (filter) {
            return this.$.dccService.searchInstitutions(filter)
        },
        getInitialVisibilityMode: function (assetsLength) {
            return assetsLength > 0 ? this.visibilityModes.LIST : this.visibilityModes.GRID
        },
        /**
        * Retrieve master list of available institutions
        * @returns {Promise<Array<object>>}
        */
        fetchInstitutions: function () {
            return this.$.dccService.fetchInstitutions()
        },
        /**
        * Retrieve master list of available institutions
        * @returns {Promise<Array<object>>}
        */
        fetchCustomerInstitutions: function (customerId, applicationNumber) {
            return this.$.dccService.fetchCustomerInstitutions(customerId, applicationNumber);
        },
        /**
        * This function will be called when we recieved selectedValues from parent
        */
        initValues: function (selectedValues) {
            if (selectedValues) {
                if (typeof selectedValues === "string") {
                    selectedValues = JSON.parse(selectedValues);
                }
                selectedValues.map(function (value) {

                }, this);
            }
        },
        handleError: function (error) {
            console.error(error)
        },
        //TODO: Changes Required
        // fetchCustomerAccounts: function (customerId) {
        //     return this.$.dccService.fetchCustomerAccounts(customerId)
        // },

        getLogoBySelectionId: function (selectedInstitutionId) {
            return _(this.institutions).filter({ institutionId: selectedInstitutionId }).get("0.logo")
        },
        showChallenge: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.CHALLENGE })
        },
        showLoading: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.LOADING })
        },
        showList: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.LIST })
            this.fire("required-condition-adjust-width", 90);
        },
        /**
         * This will raise an action (done/skipped) to move to next condition
         */
        moveToNextCondition: function (actionName) {
            var payload = {
                actionName: actionName,
                currentSelectedIndex: this.outStandingCondition.index,
                requestId: this.outStandingCondition.id
            };

            this.fire("required-condition-completed", payload);

            this.fire("required-condition-adjust-width", 90);
        }
    });
})()