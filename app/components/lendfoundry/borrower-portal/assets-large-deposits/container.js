(function () {
    /**
     * @class assetsContainer
     * @mixes alloy.lifeCycleEvents
     */
    Polymer({
        is: "assets-large-deposits-container",
        behaviors:
        [
            alloy.lifeCycleEvents,
            alloy.actions,
            alloy.visibilityController
        ],
        properties: {
            visibleButton: {
                type: Boolean,
                value: true
            },
            refreshSummaryList: {
                type: Boolean,
                value: true
            },
            writeExplaination: {
                type: Boolean,
                value: true
            },
            baseUrl: {
                type: String,
                value: null
            },
            customer: {
                type: Object,
                value: function () {
                    return {}
                }
            },
            customerId: {
                type: String,
                value: ""
            },
            accountId: {
                type: String,
                value: ""
            },
            connectionId: {
                type: String,
                value: ""
            },
            interactionId: {
                type: String,
                value: ""
            },
            invitationId: {
                type: String,
                value: ""
            },
            providerId: {
                type: String,
                value: ""
            },
            institutions: {
                type: Array,
                value: function () {
                    return []
                }
            },
            customerInstitutions: {
                type: Array,
                value: function () {
                    return []
                }
            },
            customerAccounts: {
                type: Array,
                value: function () {
                    return []
                }
            },
            largeDeposits:{
                type: Array,
                value: function () {
                    return []
                }
            },
            /**
             * @property
             */
            selectedInstitutionId: {
                type: String,
                value: ""
            },
            challenges: {
                type: Array,
                value: function () {
                    return []
                }
            },
            /** @memberOf assetsContainer */
            mfa: Object,
            institutionId: String,
            loggedInUsername: String,
            events: {
                type: Object,
                value: function () {
                    return {
                        SET_VISIBILITY_MODE: "assets-set-visibility-mode"
                    }
                },
            },
            visibilityModes: {
                type: Object,
                value: function () {
                    return {
                        INIT: "INIT",
                        ERROR: "ERROR",
                        LOADING: "LOADING",
                        FORM: "FORM",
                        GRID: "GRID",
                        CHALLENGE: "CHALLENGE",
                        LIST: "LIST",
                        UPLOADMANUALLY: "UPLOADMANUALLY"
                    }
                }
            },
            activeVisibilityMode: {
                type: String,
                value: "INIT",
                observer: '_activeVisibilityModeChanged'
            },
            error: {
                type: Object
            },
            /**
             * out-standing condition for which this component is loaded
             */
            outStandingCondition: Object,
            /**
             * application number associated with post-application
             */
            applicationNumber: String,
            /**
             * To disable buttons
             */
            inProgress: {
                type: Boolean,
                value: false
            },
            actionApplicantId: String,
            actionDocumentId: String,
             /*Need to pass to DCC endpoints*/
             institutionType: {
                type: String,
                value: 'banking'
            }
        },
        prettyPrint: function (json) {
            return JSON.stringify(json, null, 2)
        },
        /**
         * Update width of parent as per visibility mode
         */
        _activeVisibilityModeChanged: function (newValue, oldValue) {
            var widthToChange;
            switch (newValue) {
                case this.visibilityModes.LIST:
                    widthToChange = 90;
                    break;
                default:
                    widthToChange = 90;
            }
            this.fire("required-condition-adjust-width", widthToChange);
        },
        observers: [
            'initValues(selectedValues)'
        ],
        require: [
            "dccService"
        ],
        listeners: {
            "assets-set-visibility-mode": "handleSetVisibilityMode",
            "skip-condition": "handleSkip",
            "assets-large-deposits-ready":"handleLargeDepositsReady"
        },
        handleLargeDepositsReady:function(event){
            var self = this;
            if (event.detail && self.$$("#manualUploadLargeDeposits").querySelector(".ql-editor")) {
                event.detail = event.detail.trim();
                var quillHtml = '<span>To Whom it May Concern,</span><br/><span>Please accept this letter of explanation regarding the following transaction.</span><p>' + event.detail + '</p>';
                $(self.$$("#manualUploadLargeDeposits").querySelector(".ql-editor")).html('');
                $(self.$$("#manualUploadLargeDeposits").querySelector(".ql-editor")).html(quillHtml);
            }                
        },
        handleSetVisibilityMode: function (event) {
            var self = this;
            // var progressBar = this.$.progressBar;
            /** @type {string} */
            var nextVisibilityMode;
            nextVisibilityMode =
                _.get(event, "detail.actionVisibilityMode")
                || _.get(event, "detail.visibilityMode");
            var delay = 0;
            var LOADING = self.visibilityModes.LOADING;
            var LIST = self.visibilityModes.LIST;

            // If progress bar is already showing, set it to complete before changing visibility mode
            if (self.activeVisibilityMode === LOADING && nextVisibilityMode !== LOADING) {
                // Show complete progress bar before hiding
                        self.changeVisibilityMode(nextVisibilityMode)
            }

            // Ensure progress bar is started after it is visible
            // if (nextVisibilityMode === LOADING) {
            //     progressBar.start()
            // }

            //Summary
            if (nextVisibilityMode === LIST) {
                //TODO: Changes Required
                self.changeVisibilityMode(nextVisibilityMode)
                self.fire("required-condition-adjust-width", 90);
            }

            setTimeout(function () {
                self.changeVisibilityMode(nextVisibilityMode)
            }, delay);
        },
        handleSkip: function () {
            this.moveToNextCondition('skipped');
        },
        /**
        * Prepares dcc service for use
        * @returns {Promise<*>}
        */
        initDcc: function () {
            var self = this;
            return this.ensure("initDcc")
                .then(function (initDcc) {
                    return initDcc.execute()
                })
        },

        attached: function () {
            if(this.configuration.services.capacityConnect == undefined){
                throw new Error("capacityConnect  service not found");
            }
            this.baseUrl = this.configuration.services.capacityConnect;
            this.init();
        },
        init: function () {
            var self = this;

            Promise.all([
                this.initDcc()
            ]).then(function (responses) {
                self.customer = responses[0].customer;
                self.loggedInUsername = responses[0].username;
                if (!self.customerId)
                    self.customerId = self.loggedInUsername;
            })
                .then(function () {
                    self.getLargeDepositTransactions(self.customerId, self.applicationNumber)
                        .then(function (largeDeposits) {
                            self.largeDeposits = largeDeposits;
                        });
                })
                .then(function () {
                    self.fire(self.events.SET_VISIBILITY_MODE, { visibilityMode: self.visibilityModes.LIST })
                    self.fire("required-condition-adjust-width", 90);
                })
        },
        /**
        * Retrieve search list of available institutions
        * @returns {Promise<Array<object>>}
        */
        getLargeDepositTransactions: function (customerId, applicationNumber) {
            return this.$.dccService.getLargeDepositTransactions(customerId, applicationNumber)
        },
        getInitialVisibilityMode: function (assetsLength) {
            return assetsLength > 0 ? this.visibilityModes.LIST : this.visibilityModes.GRID
        },
        /**
        * This function will be called when we recieved selectedValues from parent
        */
        initValues: function (selectedValues) {
            if (selectedValues) {
                if (typeof selectedValues === "string") {
                    selectedValues = JSON.parse(selectedValues);
                }
                selectedValues.map(function (value) {

                }, this);
            }
        },
        handleError: function (error) {
            console.error(error)
        },
        getLogoBySelectionId: function (selectedInstitutionId) {
            return _(this.institutions).filter({ institutionId: selectedInstitutionId }).get("0.logo")
        },
        showLoading: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.LOADING })
        },
        showList: function () {
            this.fire(this.events.SET_VISIBILITY_MODE, { visibilityMode: this.visibilityModes.LIST })
            this.fire("required-condition-adjust-width", 90);
        },
        /**
         * This will raise an action (done/skipped) to move to next condition
         */
        moveToNextCondition: function (actionName) {
            var payload = {
                actionName: actionName,
                currentSelectedIndex: this.outStandingCondition.index,
                requestId: this.outStandingCondition.id
            };

            this.fire("required-condition-completed", payload);
            this.fire("required-condition-adjust-width", 90);

        }
    });
})()