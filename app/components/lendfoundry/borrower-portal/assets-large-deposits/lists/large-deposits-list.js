(function () {
    Polymer({
        is: 'large-deposits-list',
        properties: {
            largeDeposits:{
                type: Array,
                value: function () {
                    return []
                }
            },
            busy: Boolean,
            disableClass: { type: String, value: "" },
            inProgress: Boolean, /*This will be used to disable buttons*/
            customerId: { type: String, value: "" },
            institutionType: String,
            depositList : Array
        },
        behaviors: [eventHub, ensureDependenciesBehavior, alloy.lifeCycleEvents, alloy.actions],

        observers: ['largeDepositsUpdated(largeDeposits)'],
        ready: function(){
            var self = this;
            self.$$('#templateLargeDeposits').addEventListener("dom-change", function(event){
                self.setLargeDeposistsHTML();
            });
        },
        setLargeDeposistsHTML:function(){
            var divHtml = this.$.divLargeDeposits.innerHTML;
            var wrapped =  $("<div>" + divHtml + "</div>");
            wrapped.find('template').remove();
            var finalHtml = wrapped.html();

            this.fire("assets-large-deposits-ready",finalHtml);
        },
        computeIsEmpty: function (array) {
            if (!array)
                return true;
            return array.length === 0;
        },
        _displayLogo: function (value) {
            if (value === 0) return true;
            else return false;
        },
        largeDepositsUpdated: function (largeDeposits) {
            var self = this;
            self.set("busy", false);
            self.set("disableClass", "");
            self.depositList = largeDeposits;
        },

        getValue: function (value) {
            if (value)
                return value;
            else return "";
        },
        getDateFormattedValue: function (yearDate) {
            var year = yearDate.split(' ')[1];
            return "01/01/" + year;
        },
        handleNextClick: function (e) {
            this.set("busy", false);
            this.set("disableClass", "");
            this.fire("assets-large-deposits-submit");
        },
        formatCurrency: function (amount) {
            // This is require due to plaid amount has to come in negative and consider as a depoist.
            amount = amount < 0 ? amount * -1 : amount; 
            return '$' + amount.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            

        },
        incSeqNo:function(seqNo,inc)
        {
            return seqNo + inc;
        },
        getLatestCategory:function (category)
        {
         if(category)
                return category[category.length - 1];
            else return '';
        },
        getFormattedDate:function(date)
        {
            if(date)
            {
                return moment
                .tz(date, window.tenant.timezone)
                .format("MM/DD/YYYY");

                //return date.split('T')[0];
            }
            else return date;
        }
    });
})()