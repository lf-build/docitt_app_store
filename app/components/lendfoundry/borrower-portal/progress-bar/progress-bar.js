Polymer(/** @class borrowerProgressBar */{
  is: "borrower-progress-bar",

  behaviors: [alloy.lifeCycleEvents],

  properties: {
    /**
     * @type {number}
     * @memberOf borrowerProgressBar
     */
    progress: {
      type: Number,
      observer: "_handleProgressChanged"
    },

    config: {
      type: Object,
      value: function () {
        /**
         * @type {Object}
         * @memberof borrowerProgressBar
         */
        return {
          maxProgress: 100,
          minProgress: 0,
          minStep: .5,
          transitionDuration: 250,
          updateInterval: 250
        }
      }
    }
  },

  ready: function () {
    this.style.transition =
      _.template("all ${transitionDuration}ms ease-in-out")({
        transitionDuration: this.config.transitionDuration
      })

    this.reset()
  },

  start: function () {
    this.reset()
    this._scheduleUpdates()
  },

  stop: function () {
    if (this.scheduledProgressUpdates)
      clearInterval(this.scheduledProgressUpdates)
  },

  finish: function () {
    return new Promise(function (resolve) {
      var transitionEnded = function () {
        resolve()
        this.removeEventListener("transitionend", transitionEnded)
      }.bind(this)

      this.addEventListener("transitionend", transitionEnded)
      this.progress = this.config.maxProgress
    }.bind(this))
  },

  reset: function () {
    this.progress = this.config.minProgress
  },

  _scheduleUpdates: function () {
    this.scheduledProgressUpdates = setInterval(function () {
      this._update(1 / 2000 * this.progress * this.progress * -1 + (2 * Math.random()))
    }.bind(this), this.config.updateInterval)
  },

  _handleProgressChanged: function (progress) {
    this.style.width = _.clamp(progress, this.config.minProgress, this.config.maxProgress) + "%"

    if (progress >= this.config.maxProgress)
      this.stop()
  },

  _update: function (step) {
    this.progress += step
  },
})