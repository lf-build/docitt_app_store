# Docitt

### Links
[Issues](https://projects.codebase.guru/issues/?jql=project%20%3D%20DOC)

[SRS](https://sigmainfosolutionsinc.app.box.com/s/c1po66pgscc22m4b4wb6jvbs78e4gmqu)

[UI Style Board](https://xd.adobe.com/view/e033fa78-8008-4687-8669-6276139a261d/)

[Progress Bar Style Board](https://xd.adobe.com/view/5089b1ef-5895-41fa-9493-dffbb2133e68/)

[Lender Dashboard Designs](https://xd.adobe.com/view/9848453d-a5cf-4724-b21b-9589606fc3a9/)

### Develop
From the repository root:
`npm install`

On Linux/OSX: 
`npm run dev`

On Windows:
`npm run dev-win`

### Run Tests
To run e2e tests use one of the following:

#### Borrower portal tests:
[#/get-started](https://docitt.qa.lendfoundry.com:9005/#/get-started)
`nightwatch --env docitt-borrower --group get-started`