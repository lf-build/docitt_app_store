module.exports = {
    'Login with valid credentials': function (browser) {
        var login = browser.page.login().navigate();

        login.validate()

        login.submitCredentials(
            browser.globals.valid.username,
            browser.globals.valid.password
        );

        login.waitForElementNotVisible('@errorMessage', 1000)

        browser.pause(5000)
        browser.assert.urlContains('dashboard')
    },
    'Login with invalid credentials': function (browser) {
        var login = browser.page.login().navigate();

        login.validate()
        login.submitCredentials(
            browser.globals.invalid.username,
            browser.globals.invalid.password
        )

        login.waitForElementVisible('@errorMessage', 1000)

        browser.pause(5000)

        browser.assert.urlContains('login')


    }
};