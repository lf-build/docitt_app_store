module.exports = {
    url: function () {
        return this.api.launch_url + '#/sign-up';
    },
    elements: {
        securityLogin: 'security-login',
        errorMessage: '#error-message',
        username: '#username',
        password: '#password',
        submit: '#log-in'
    },
    commands: [{
        validate: function () {
            this.waitForElementVisible('@securityLogin', 1000)
                .assert.visible('@username')
                .assert.visible('@password')
                .assert.visible('@submit')
        },
        submitCredentials: function (username, password) {
            this.setValue('@username', username);
            this.setValue('@password', password);
            this.click('@submit');
        },
    }]
}