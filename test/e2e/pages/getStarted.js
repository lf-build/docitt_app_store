module.exports = {
    url: function () {
        return this.api.launch_url + '#/get-started';
    },
    elements: {
        signUpButton: {
            selector: '//*[@id="micro-app-host"]/get-started/div/div/button[1]',
            locateStrategy: 'xpath'

        },
        logInButton: {
            selector: '//*[@id="micro-app-host"]/get-started/div/div/button[2]',
            locateStrategy: 'xpath'
        }
    },
    commands: [
        {
            clickSignUp: function () {
                var signUpButton = '@signUpButton'

                this.waitForElementVisible(signUpButton, 2000)
                    .click(signUpButton)
            },
            clickLogIn: function () {
                var logInButton = '@logInButton'

                this.waitForElementVisible(logInButton, 2000)
                    .click(logInButton)
            }
        }
    ]
}