module.exports = {
    'Click Sign Up': function (client) {
        var getStarted = client.page.getStarted().navigate();

        client.waitForElementVisible('body', 1000)

        getStarted.clickSignUp();

        client.end();
    },
    'Click Log In': function (client) {
        var getStarted = client.page.getStarted().navigate();

        client.waitForElementVisible('body', 1000)

        getStarted.clickLogIn();

        client.end();
    }
};