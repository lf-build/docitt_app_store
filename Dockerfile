FROM node:latest as builder

RUN npm install -g grunt-cli

WORKDIR /src

ADD /package.json /src/package.json

RUN npm install

ADD /app /src/app

ADD /Gruntfile.js /src/Gruntfile.js

RUN grunt  build

FROM nginx:1.15-alpine

ADD nginx.conf /etc/nginx/nginx.conf
ADD crossdomain.xml /src/dist/crossdomain.xml
COPY --from=builder /src/dist/components/. /src/dist/app

EXPOSE 5000

ENTRYPOINT nginx -g 'daemon off;'