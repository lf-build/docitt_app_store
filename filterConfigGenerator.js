var filters = {
    "initiated": [
        "200.01"
    ],
    "offerGenerated": [
        "300.01"
    ],
    "underwritingReview": [
        "300.03"
    ],
    "loanAgreementGenerated": [
        "300.05"
    ],
    "loanAgreementSigned": [
        "300.06"
    ],
    "fundingReview": [
        "400.01"
    ],
    "fundingApproved": [
        "400.02"
    ],
    "funded": [
        "400.03"
    ],
    "declined": [
        "500.01"
    ],
    "expired": [
        "500.02"
    ],
    "cancelled": [
        "500.03"
    ],
    "suspectedFraud": [
        "500.04"
    ],
    "fraud": [
        "500.05"
    ],
    "declinedDuplicate": [
        "500.06"
    ],
    "inService": [
        "600.01"
    ],
    "paidOff": [
        "600.02"
    ],
    "writtenOff": [
        "600.03"
    ],
    "chargedOff": [
        "600.03"
    ]
};

var newFilters = Object.keys(filters).reduce(function (newFilters, key) {
    newFilters[key] = {
        type: "status",
        values: filters[key]
    };
    return newFilters;
}, {});

console.log(JSON.stringify(newFilters));