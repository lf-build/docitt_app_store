var gulp = require("gulp");
var browserSync = require("browser-sync");

// Watch files for changes & reload
gulp.task('test', function () {
    browserSync.init({
        port: 5000,
        server: {
            baseDir: './'
        },
        startPath: "/test/index.html"
    });

    gulp.watch(
        [
            "./components/**/*.*",
            "./test/**/*.*"
        ]
    ).on("change", browserSync.reload)
});

gulp.task('dev', function () {
    browserSync.init({
        port: 5000,
        server: {
            baseDir: './'
        },
        startPath: "http://192.168.1.9:9012"
    });

    gulp.watch(["./components/**/*.*"]).on("change", browserSync.reload);
});